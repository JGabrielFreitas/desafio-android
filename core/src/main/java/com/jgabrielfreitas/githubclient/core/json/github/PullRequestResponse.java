package com.jgabrielfreitas.githubclient.core.json.github;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by JGabrielFreitas on 20/05/16.
 */
public class PullRequestResponse extends BaseApiResponse implements Parcelable {

    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("html_url")
    @Expose
    public String htmlUrl;
    @SerializedName("diff_url")
    @Expose
    public String diffUrl;
    @SerializedName("patch_url")
    @Expose
    public String patchUrl;
    @SerializedName("issue_url")
    @Expose
    public String issueUrl;
    @SerializedName("number")
    @Expose
    public Integer number;
    @SerializedName("state")
    @Expose
    public String state;
    @SerializedName("locked")
    @Expose
    public Boolean locked;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("user")
    @Expose
    public User user;
    @SerializedName("body")
    @Expose
    public String body;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("closed_at")
    @Expose
    public String closedAt;
    @SerializedName("merged_at")
    @Expose
    public String mergedAt;
    @SerializedName("merge_commit_sha")
    @Expose
    public String mergeCommitSha;
    @SerializedName("assignee")
    @Expose
    public String assignee;
    @SerializedName("milestone")
    @Expose
    public String milestone;
    @SerializedName("commits_url")
    @Expose
    public String commitsUrl;
    @SerializedName("review_comments_url")
    @Expose
    public String reviewCommentsUrl;
    @SerializedName("review_comment_url")
    @Expose
    public String reviewCommentUrl;
    @SerializedName("comments_url")
    @Expose
    public String commentsUrl;
    @SerializedName("statuses_url")
    @Expose
    public String statusesUrl;
    @SerializedName("head")
    @Expose
    public Head head;
    @SerializedName("base")
    @Expose
    public Base base;
    @SerializedName("_links")
    @Expose
    public Links Links;

    public String getUrl() {
        return url;
    }

    public Integer getId() {
        return id;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public String getDiffUrl() {
        return diffUrl;
    }

    public String getPatchUrl() {
        return patchUrl;
    }

    public String getIssueUrl() {
        return issueUrl;
    }

    public Integer getNumber() {
        return number;
    }

    public String getState() {
        return state;
    }

    public Boolean getLocked() {
        return locked;
    }

    public String getTitle() {
        return title;
    }

    public User getUser() {
        return user;
    }

    public String getBody() {
        return body;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public Object getClosedAt() {
        return closedAt;
    }

    public Object getMergedAt() {
        return mergedAt;
    }

    public String getMergeCommitSha() {
        return mergeCommitSha;
    }

    public Object getAssignee() {
        return assignee;
    }

    public Object getMilestone() {
        return milestone;
    }

    public String getCommitsUrl() {
        return commitsUrl;
    }

    public String getReviewCommentsUrl() {
        return reviewCommentsUrl;
    }

    public String getReviewCommentUrl() {
        return reviewCommentUrl;
    }

    public String getCommentsUrl() {
        return commentsUrl;
    }

    public String getStatusesUrl() {
        return statusesUrl;
    }

    public Head getHead() {
        return head;
    }

    public Base getBase() {
        return base;
    }

    public com.jgabrielfreitas.githubclient.core.json.github.Links getLinks() {
        return Links;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.url);
        dest.writeValue(this.id);
        dest.writeString(this.htmlUrl);
        dest.writeString(this.diffUrl);
        dest.writeString(this.patchUrl);
        dest.writeString(this.issueUrl);
        dest.writeValue(this.number);
        dest.writeString(this.state);
        dest.writeValue(this.locked);
        dest.writeString(this.title);
        dest.writeParcelable(this.user, flags);
        dest.writeString(this.body);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeString(this.closedAt);
        dest.writeString(this.mergedAt);
        dest.writeString(this.mergeCommitSha);
        dest.writeString(this.assignee);
        dest.writeString(this.milestone);
        dest.writeString(this.commitsUrl);
        dest.writeString(this.reviewCommentsUrl);
        dest.writeString(this.reviewCommentUrl);
        dest.writeString(this.commentsUrl);
        dest.writeString(this.statusesUrl);
        dest.writeParcelable(this.head, flags);
        dest.writeParcelable(this.base, flags);
        dest.writeParcelable(this.Links, flags);
    }

    public PullRequestResponse() {
    }

    protected PullRequestResponse(Parcel in) {
        this.url = in.readString();
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.htmlUrl = in.readString();
        this.diffUrl = in.readString();
        this.patchUrl = in.readString();
        this.issueUrl = in.readString();
        this.number = (Integer) in.readValue(Integer.class.getClassLoader());
        this.state = in.readString();
        this.locked = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.title = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.body = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.closedAt = in.readString();
        this.mergedAt = in.readString();
        this.mergeCommitSha = in.readString();
        this.assignee = in.readString();
        this.milestone = in.readString();
        this.commitsUrl = in.readString();
        this.reviewCommentsUrl = in.readString();
        this.reviewCommentUrl = in.readString();
        this.commentsUrl = in.readString();
        this.statusesUrl = in.readString();
        this.head = in.readParcelable(Head.class.getClassLoader());
        this.base = in.readParcelable(Base.class.getClassLoader());
        this.Links = in.readParcelable(com.jgabrielfreitas.githubclient.core.json.github.Links.class.getClassLoader());
    }

    public static final Creator<PullRequestResponse> CREATOR = new Creator<PullRequestResponse>() {
        @Override
        public PullRequestResponse createFromParcel(Parcel source) {
            return new PullRequestResponse(source);
        }

        @Override
        public PullRequestResponse[] newArray(int size) {
            return new PullRequestResponse[size];
        }
    };
}
