package com.jgabrielfreitas.githubclient.core.json.github;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public class Commits extends Href implements Parcelable {
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public Commits() {
    }

    protected Commits(Parcel in) {
    }

    public static final Parcelable.Creator<Commits> CREATOR = new Parcelable.Creator<Commits>() {
        @Override
        public Commits createFromParcel(Parcel source) {
            return new Commits(source);
        }

        @Override
        public Commits[] newArray(int size) {
            return new Commits[size];
        }
    };
}
