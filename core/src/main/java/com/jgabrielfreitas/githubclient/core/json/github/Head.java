package com.jgabrielfreitas.githubclient.core.json.github;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public class Head implements Parcelable {

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("ref")
    @Expose
    private String ref;
    @SerializedName("sha")
    @Expose
    private String sha;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("repo")
    @Expose
    private RepositoryResponse repo;

    public String getLabel() {
        return label;
    }

    public String getRef() {
        return ref;
    }

    public String getSha() {
        return sha;
    }

    public User getUser() {
        return user;
    }

    public RepositoryResponse getRepo() {
        return repo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.label);
        dest.writeString(this.ref);
        dest.writeString(this.sha);
        dest.writeParcelable(this.user, flags);
        dest.writeParcelable(this.repo, flags);
    }

    public Head() {
    }

    protected Head(Parcel in) {
        this.label = in.readString();
        this.ref = in.readString();
        this.sha = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.repo = in.readParcelable(RepositoryResponse.class.getClassLoader());
    }

    public static final Parcelable.Creator<Head> CREATOR = new Parcelable.Creator<Head>() {
        @Override
        public Head createFromParcel(Parcel source) {
            return new Head(source);
        }

        @Override
        public Head[] newArray(int size) {
            return new Head[size];
        }
    };
}
