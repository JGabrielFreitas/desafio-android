package com.jgabrielfreitas.githubclient.core.client;

import android.content.Context;

import com.jgabrielfreitas.githubclient.core.activities.CoreBaseActivity;
import com.jgabrielfreitas.githubclient.core.reactive.Utils;

import retrofit2.Retrofit;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public abstract class BaseClient<Client> {

    protected Retrofit retrofit;
    protected Context context;
    protected String baseUrl;
    protected Class<Client> clientClass;
    protected boolean executeFeedback;

    public BaseClient(Context context, String baseUrl, Class<Client> clientClass) {
        this.context = context;
        this.baseUrl = baseUrl;
        this.clientClass = clientClass;
        this.executeFeedback = false;
    }

    public BaseClient(Context context, String baseUrl, Class<Client> clientClass, boolean executeFeedback) {
        this(context, baseUrl, clientClass);
        this.executeFeedback = executeFeedback;
    }

    public Observable subscribeAndObserveOn(Observable observable) {
        executeFeedback();
        retrofit = Utils.createRetrofit(baseUrl);

        //default pattern for an rx execution
        observable = observable.subscribeOn(Schedulers.newThread());
        observable = observable.observeOn(AndroidSchedulers.mainThread());

        return observable;
    }

    protected void executeFeedback() {

        if (executeFeedback && context instanceof CoreBaseActivity)
            ((CoreBaseActivity)context).showProgress();
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Client getClient() {

        if (retrofit == null)
            retrofit = Utils.createRetrofit(baseUrl);

        return retrofit.create(clientClass);
    }

    public boolean isExecuteFeedback() {
        return executeFeedback;
    }

    public void setExecuteFeedback(boolean executeFeedback) {
        this.executeFeedback = executeFeedback;
    }

    public int getResponseCode() {
        return 0;
    }

}
