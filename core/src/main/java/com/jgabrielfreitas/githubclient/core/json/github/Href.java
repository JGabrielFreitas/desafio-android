package com.jgabrielfreitas.githubclient.core.json.github;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public abstract class Href {

    @SerializedName("href")
    @Expose
    private String href;

    public String getHref() {
        return href;
    }
}
