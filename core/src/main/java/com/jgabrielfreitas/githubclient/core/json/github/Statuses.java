package com.jgabrielfreitas.githubclient.core.json.github;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public class Statuses extends Href implements Parcelable {
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public Statuses() {
    }

    protected Statuses(Parcel in) {
    }

    public static final Parcelable.Creator<Statuses> CREATOR = new Parcelable.Creator<Statuses>() {
        @Override
        public Statuses createFromParcel(Parcel source) {
            return new Statuses(source);
        }

        @Override
        public Statuses[] newArray(int size) {
            return new Statuses[size];
        }
    };
}
