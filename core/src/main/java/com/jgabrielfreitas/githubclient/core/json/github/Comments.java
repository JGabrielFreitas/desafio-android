package com.jgabrielfreitas.githubclient.core.json.github;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public class Comments extends Href implements Parcelable {

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public Comments() {
    }

    protected Comments(Parcel in) {
    }

    public static final Parcelable.Creator<Comments> CREATOR = new Parcelable.Creator<Comments>() {
        @Override
        public Comments createFromParcel(Parcel source) {
            return new Comments(source);
        }

        @Override
        public Comments[] newArray(int size) {
            return new Comments[size];
        }
    };
}
