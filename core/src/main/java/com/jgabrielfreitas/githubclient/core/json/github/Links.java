package com.jgabrielfreitas.githubclient.core.json.github;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public class Links implements Parcelable {

    @SerializedName("self")
    @Expose
    private Self self;
    @SerializedName("html")
    @Expose
    private Html html;
    @SerializedName("issue")
    @Expose
    private Issue issue;
    @SerializedName("comments")
    @Expose
    private Comments comments;
    @SerializedName("review_comments")
    @Expose
    private ReviewComments reviewComments;
    @SerializedName("review_comment")
    @Expose
    private ReviewComment reviewComment;
    @SerializedName("commits")
    @Expose
    private Commits commits;
    @SerializedName("statuses")
    @Expose
    private Statuses statuses;

    public Self getSelf() {
        return self;
    }

    public Html getHtml() {
        return html;
    }

    public Issue getIssue() {
        return issue;
    }

    public Comments getComments() {
        return comments;
    }

    public ReviewComments getReviewComments() {
        return reviewComments;
    }

    public ReviewComment getReviewComment() {
        return reviewComment;
    }

    public Commits getCommits() {
        return commits;
    }

    public Statuses getStatuses() {
        return statuses;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.self, flags);
        dest.writeParcelable(this.html, flags);
        dest.writeParcelable(this.issue, flags);
        dest.writeParcelable(this.comments, flags);
        dest.writeParcelable(this.reviewComments, flags);
        dest.writeParcelable(this.reviewComment, flags);
        dest.writeParcelable(this.commits, flags);
        dest.writeParcelable(this.statuses, flags);
    }

    public Links() {
    }

    protected Links(Parcel in) {
        this.self = in.readParcelable(Self.class.getClassLoader());
        this.html = in.readParcelable(Html.class.getClassLoader());
        this.issue = in.readParcelable(Issue.class.getClassLoader());
        this.comments = in.readParcelable(Comments.class.getClassLoader());
        this.reviewComments = in.readParcelable(ReviewComments.class.getClassLoader());
        this.reviewComment = in.readParcelable(ReviewComment.class.getClassLoader());
        this.commits = in.readParcelable(Commits.class.getClassLoader());
        this.statuses = in.readParcelable(Statuses.class.getClassLoader());
    }

    public static final Parcelable.Creator<Links> CREATOR = new Parcelable.Creator<Links>() {
        @Override
        public Links createFromParcel(Parcel source) {
            return new Links(source);
        }

        @Override
        public Links[] newArray(int size) {
            return new Links[size];
        }
    };
}
