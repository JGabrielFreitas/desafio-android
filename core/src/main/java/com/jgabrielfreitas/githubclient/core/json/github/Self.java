package com.jgabrielfreitas.githubclient.core.json.github;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public class Self extends Href implements Parcelable {


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public Self() {
    }

    protected Self(Parcel in) {
    }

    public static final Parcelable.Creator<Self> CREATOR = new Parcelable.Creator<Self>() {
        @Override
        public Self createFromParcel(Parcel source) {
            return new Self(source);
        }

        @Override
        public Self[] newArray(int size) {
            return new Self[size];
        }
    };
}
