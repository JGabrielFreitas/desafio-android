package com.jgabrielfreitas.githubclient.core.client;

import android.content.Context;

import com.jgabrielfreitas.githubclient.core.BuildConfig;
import com.jgabrielfreitas.githubclient.core.json.github.PullRequestResponse;
import com.jgabrielfreitas.githubclient.core.json.github.RepositoryResponse;
import com.jgabrielfreitas.githubclient.core.retrofit.GithubClientInterface;

import java.util.List;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public class RxGithubClient extends BaseClient<GithubClientInterface> implements GithubClientInterface {

    public RxGithubClient(Context context) {
        super(context, BuildConfig.GITHUB_API_URL, GithubClientInterface.class);
    }

    public RxGithubClient(Context context, String baseUrl, Class<GithubClientInterface> githubClientClass, boolean executeFeedback) {
        super(context, baseUrl, githubClientClass, executeFeedback);
    }

    public Observable<Response<List<RepositoryResponse>>> getRepositoryList(String user, int page) {
        return subscribeAndObserveOn(getClient().getRepositoryList(user, page));
    }

    public Observable<List<PullRequestResponse>> getPullRequests(String user, String repo) {
        return subscribeAndObserveOn(getClient().getPullRequests(user, repo));
    }
}
