package com.jgabrielfreitas.githubclient.core.json.github;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public class ReviewComment extends Href implements Parcelable {
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public ReviewComment() {
    }

    protected ReviewComment(Parcel in) {
    }

    public static final Parcelable.Creator<ReviewComment> CREATOR = new Parcelable.Creator<ReviewComment>() {
        @Override
        public ReviewComment createFromParcel(Parcel source) {
            return new ReviewComment(source);
        }

        @Override
        public ReviewComment[] newArray(int size) {
            return new ReviewComment[size];
        }
    };
}
