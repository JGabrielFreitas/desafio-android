package com.jgabrielfreitas.githubclient.core.json.github;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public class ReviewComments extends Href implements Parcelable {
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public ReviewComments() {
    }

    protected ReviewComments(Parcel in) {
    }

    public static final Parcelable.Creator<ReviewComments> CREATOR = new Parcelable.Creator<ReviewComments>() {
        @Override
        public ReviewComments createFromParcel(Parcel source) {
            return new ReviewComments(source);
        }

        @Override
        public ReviewComments[] newArray(int size) {
            return new ReviewComments[size];
        }
    };
}
