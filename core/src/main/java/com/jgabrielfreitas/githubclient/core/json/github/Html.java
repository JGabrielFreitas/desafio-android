package com.jgabrielfreitas.githubclient.core.json.github;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public class Html extends Href implements Parcelable {
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public Html() {
    }

    protected Html(Parcel in) {
    }

    public static final Parcelable.Creator<Html> CREATOR = new Parcelable.Creator<Html>() {
        @Override
        public Html createFromParcel(Parcel source) {
            return new Html(source);
        }

        @Override
        public Html[] newArray(int size) {
            return new Html[size];
        }
    };
}
