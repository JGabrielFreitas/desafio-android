package com.jgabrielfreitas.githubclient.core.reactive;

import android.content.Context;

import com.jgabrielfreitas.githubclient.core.activities.CoreBaseActivity;

import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public class RetrofitSubscriber<T> extends FeedbackSubscriber<T> {

    private T error;
    private String errorBody;
    private Response<?> response;
    private boolean showDefaultError;

    public RetrofitSubscriber(Context context) {
        super(context);
        this.showDefaultError = true;
    }

    public RetrofitSubscriber(Context context, boolean showDefaultError) {

        super(context);
        this.showDefaultError = showDefaultError;
    }

    public RetrofitSubscriber(Context context, boolean showDefaultError, boolean executeFeedback) {

        this(context, showDefaultError);
        super.setExecuteFeedback(executeFeedback);
    }

    @Override
    public void onError(Throwable t) {

        super.onError(t);

        getResponse(t);
        showDefaultError(response, t);
    }

    private void getResponse(Throwable t) {

        if (t instanceof HttpException)
            response = ((HttpException) t).response();
    }

    private void showDefaultError(Response response, Throwable t) {

        if (showDefaultError)
            showDefaultError(t);

        if (response == null || !(response.code() >= 300 && response.code() < 500 && response.code() != 404))
            logError(response, t);
    }

    private void showDefaultError(Throwable t) {

        if (getContext() instanceof CoreBaseActivity)
            ((CoreBaseActivity)getContext()).catchDefaultException(t);
    }

    private void logError(Response response, Throwable t) {

        try {

            Throwable exception = t;

            if (response != null) {

                String raw = response.raw().toString();
                exception = new Exception("Headers: " + raw + "\n" + "Body: " + errorBody);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public T deserializeError() {

        return error;
    }

    public boolean isShowDefaultError() {
        return showDefaultError;
    }

    public void setShowDefaultError(boolean showDefaultError) {
        this.showDefaultError = showDefaultError;
    }

    public Response<?> getResponse() {
        return response;
    }

    public void setResponse(Response<?> response) {
        this.response = response;
    }

    public T getError() {
        return error;
    }

    public void setError(T error) {
        this.error = error;
    }
}
