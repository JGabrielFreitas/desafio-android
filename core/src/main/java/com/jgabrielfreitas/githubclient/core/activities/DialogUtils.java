package com.jgabrielfreitas.githubclient.core.activities;

import android.content.Context;

import com.jgabrielfreitas.callbacks.OnPositiveListener;
import com.jgabrielfreitas.dialog.SimpleAlertDialog;
import com.jgabrielfreitas.githubclient.core.R;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public class DialogUtils {

    public static void createDialogWithMessage(Context context, String string) {
        new SimpleAlertDialog(context).setPositiveButton(context.getString(R.string.close)).setTitle(context.getString(R.string.warning)).setContent(string).show();
    }

    public static void createDialogWithMessage(Context context, String string, OnPositiveListener onPositiveListener) {
        new SimpleAlertDialog(context).setPositiveButton(context.getString(R.string.close), onPositiveListener).setTitle(context.getString(R.string.warning)).setContent(string).show();
    }
}
