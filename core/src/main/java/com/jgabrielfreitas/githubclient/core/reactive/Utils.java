package com.jgabrielfreitas.githubclient.core.reactive;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public class Utils {


    public static Retrofit createRetrofit(String baseUrl) {
        return new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create(createGson())).baseUrl(baseUrl).addCallAdapterFactory(RxJavaCallAdapterFactory.create()).client(getOkHttpClient()).build();
    }

    public static Gson createGson() {

        return new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
    }

    public static OkHttpClient getOkHttpClient() {

        // create interceptor to show all request and response in log (for debug)
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor)
                                                        .readTimeout(30, TimeUnit.SECONDS)
                                                        .build();
        return client;
    }
}
