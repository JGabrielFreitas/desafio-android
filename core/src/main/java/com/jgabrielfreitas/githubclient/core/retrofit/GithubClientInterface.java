package com.jgabrielfreitas.githubclient.core.retrofit;

import com.jgabrielfreitas.githubclient.core.json.github.PullRequestResponse;
import com.jgabrielfreitas.githubclient.core.json.github.RepositoryResponse;

import java.util.List;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public interface GithubClientInterface {

    @GET("users/{user}/repos")
    Observable<Response<List<RepositoryResponse>>> getRepositoryList(@Path("user") String user, @Query("page") int page);

    @GET("repos/{user}/{repo}/pulls")
    Observable<List<PullRequestResponse>> getPullRequests(@Path("user") String user, @Path("repo") String repo);

}
