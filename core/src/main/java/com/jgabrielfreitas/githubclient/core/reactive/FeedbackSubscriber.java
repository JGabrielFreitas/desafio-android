package com.jgabrielfreitas.githubclient.core.reactive;

import android.content.Context;

import com.jgabrielfreitas.githubclient.core.activities.CoreBaseActivity;

import rx.Subscriber;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public abstract class FeedbackSubscriber<T> extends Subscriber<T> {

    private Context context;
    private boolean executeFeedback;

    public FeedbackSubscriber(Context context) {

        this.context = context;
        executeFeedback = true;
    }

    public FeedbackSubscriber(Context context, boolean executeFeedback) {

        this(context);
        this.executeFeedback = executeFeedback;
    }

    @Override
    public void onCompleted() {

        removeFeedback();
    }

    @Override
    public void onError(Throwable e) {

        removeFeedback();
    }

    @Override
    public void onNext(T t) {

        removeFeedback();
    }

    private void removeFeedback() {

        if (executeFeedback && context instanceof CoreBaseActivity)
            ((CoreBaseActivity)context).removeProgress();
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public boolean isExecuteFeedback() {
        return executeFeedback;
    }

    public void setExecuteFeedback(boolean executeFeedback) {
        this.executeFeedback = executeFeedback;
    }
}