package com.jgabrielfreitas.githubclient.core.activities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;

import com.jgabrielfreitas.githubclient.core.R;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public abstract class CoreBaseActivity extends AppCompatActivity {

    protected ProgressDialog progressDialog;

    public void showProgress() {

        if (progressDialog == null)
            progressDialog = ProgressDialog.show(this, "", getString(R.string.loading), false, false);
    }

    public void removeProgress() {

        if (progressDialog != null) {

            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void catchDefaultException(Throwable e) {

        showErrorDialog();
    }

    protected void showErrorDialog() {

        removeProgress();
        DialogUtils.createDialogWithMessage(this, getString(R.string.default_error_message));
    }

    public void createSimpleDialog(String message) {
        DialogUtils.createDialogWithMessage(this, message);
    }
}
