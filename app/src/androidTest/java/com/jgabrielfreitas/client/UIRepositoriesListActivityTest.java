package com.jgabrielfreitas.client;

import android.content.Intent;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jgabrielfreitas.client.activities.RepositoriesListActivity;
import com.jgabrielfreitas.client.util.Util;
import com.jgabrielfreitas.client.wrappers.RepositoryWrapper;
import com.jgabrielfreitas.githubclient.core.json.github.RepositoryResponse;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.registerIdlingResources;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by JGabrielFreitas on 21/05/16.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class UIRepositoriesListActivityTest {

    public final String OCTOCAT_REPO_MOCK = "[\n" +
            "  {\n" +
            "    \"id\": 18221276,\n" +
            "    \"name\": \"git-consortium\",\n" +
            "    \"full_name\": \"octocat/git-consortium\",\n" +
            "    \"owner\": {\n" +
            "      \"login\": \"octocat\",\n" +
            "      \"id\": 583231,\n" +
            "      \"avatar_url\": \"https://avatars.githubusercontent.com/u/583231?v=3\",\n" +
            "      \"gravatar_id\": \"\",\n" +
            "      \"url\": \"https://api.github.com/users/octocat\",\n" +
            "      \"html_url\": \"https://github.com/octocat\",\n" +
            "      \"followers_url\": \"https://api.github.com/users/octocat/followers\",\n" +
            "      \"following_url\": \"https://api.github.com/users/octocat/following{/other_user}\",\n" +
            "      \"gists_url\": \"https://api.github.com/users/octocat/gists{/gist_id}\",\n" +
            "      \"starred_url\": \"https://api.github.com/users/octocat/starred{/owner}{/repo}\",\n" +
            "      \"subscriptions_url\": \"https://api.github.com/users/octocat/subscriptions\",\n" +
            "      \"organizations_url\": \"https://api.github.com/users/octocat/orgs\",\n" +
            "      \"repos_url\": \"https://api.github.com/users/octocat/repos\",\n" +
            "      \"events_url\": \"https://api.github.com/users/octocat/events{/privacy}\",\n" +
            "      \"received_events_url\": \"https://api.github.com/users/octocat/received_events\",\n" +
            "      \"type\": \"User\",\n" +
            "      \"site_admin\": false\n" +
            "    },\n" +
            "    \"private\": false,\n" +
            "    \"html_url\": \"https://github.com/octocat/git-consortium\",\n" +
            "    \"description\": \"This repo is for demonstration purposes only.\",\n" +
            "    \"fork\": false,\n" +
            "    \"url\": \"https://api.github.com/repos/octocat/git-consortium\",\n" +
            "    \"forks_url\": \"https://api.github.com/repos/octocat/git-consortium/forks\",\n" +
            "    \"keys_url\": \"https://api.github.com/repos/octocat/git-consortium/keys{/key_id}\",\n" +
            "    \"collaborators_url\": \"https://api.github.com/repos/octocat/git-consortium/collaborators{/collaborator}\",\n" +
            "    \"teams_url\": \"https://api.github.com/repos/octocat/git-consortium/teams\",\n" +
            "    \"hooks_url\": \"https://api.github.com/repos/octocat/git-consortium/hooks\",\n" +
            "    \"issue_events_url\": \"https://api.github.com/repos/octocat/git-consortium/issues/events{/number}\",\n" +
            "    \"events_url\": \"https://api.github.com/repos/octocat/git-consortium/events\",\n" +
            "    \"assignees_url\": \"https://api.github.com/repos/octocat/git-consortium/assignees{/user}\",\n" +
            "    \"branches_url\": \"https://api.github.com/repos/octocat/git-consortium/branches{/branch}\",\n" +
            "    \"tags_url\": \"https://api.github.com/repos/octocat/git-consortium/tags\",\n" +
            "    \"blobs_url\": \"https://api.github.com/repos/octocat/git-consortium/git/blobs{/sha}\",\n" +
            "    \"git_tags_url\": \"https://api.github.com/repos/octocat/git-consortium/git/tags{/sha}\",\n" +
            "    \"git_refs_url\": \"https://api.github.com/repos/octocat/git-consortium/git/refs{/sha}\",\n" +
            "    \"trees_url\": \"https://api.github.com/repos/octocat/git-consortium/git/trees{/sha}\",\n" +
            "    \"statuses_url\": \"https://api.github.com/repos/octocat/git-consortium/statuses/{sha}\",\n" +
            "    \"languages_url\": \"https://api.github.com/repos/octocat/git-consortium/languages\",\n" +
            "    \"stargazers_url\": \"https://api.github.com/repos/octocat/git-consortium/stargazers\",\n" +
            "    \"contributors_url\": \"https://api.github.com/repos/octocat/git-consortium/contributors\",\n" +
            "    \"subscribers_url\": \"https://api.github.com/repos/octocat/git-consortium/subscribers\",\n" +
            "    \"subscription_url\": \"https://api.github.com/repos/octocat/git-consortium/subscription\",\n" +
            "    \"commits_url\": \"https://api.github.com/repos/octocat/git-consortium/commits{/sha}\",\n" +
            "    \"git_commits_url\": \"https://api.github.com/repos/octocat/git-consortium/git/commits{/sha}\",\n" +
            "    \"comments_url\": \"https://api.github.com/repos/octocat/git-consortium/comments{/number}\",\n" +
            "    \"issue_comment_url\": \"https://api.github.com/repos/octocat/git-consortium/issues/comments{/number}\",\n" +
            "    \"contents_url\": \"https://api.github.com/repos/octocat/git-consortium/contents/{+path}\",\n" +
            "    \"compare_url\": \"https://api.github.com/repos/octocat/git-consortium/compare/{base}...{head}\",\n" +
            "    \"merges_url\": \"https://api.github.com/repos/octocat/git-consortium/merges\",\n" +
            "    \"archive_url\": \"https://api.github.com/repos/octocat/git-consortium/{archive_format}{/ref}\",\n" +
            "    \"downloads_url\": \"https://api.github.com/repos/octocat/git-consortium/downloads\",\n" +
            "    \"issues_url\": \"https://api.github.com/repos/octocat/git-consortium/issues{/number}\",\n" +
            "    \"pulls_url\": \"https://api.github.com/repos/octocat/git-consortium/pulls{/number}\",\n" +
            "    \"milestones_url\": \"https://api.github.com/repos/octocat/git-consortium/milestones{/number}\",\n" +
            "    \"notifications_url\": \"https://api.github.com/repos/octocat/git-consortium/notifications{?since,all,participating}\",\n" +
            "    \"labels_url\": \"https://api.github.com/repos/octocat/git-consortium/labels{/name}\",\n" +
            "    \"releases_url\": \"https://api.github.com/repos/octocat/git-consortium/releases{/id}\",\n" +
            "    \"deployments_url\": \"https://api.github.com/repos/octocat/git-consortium/deployments\",\n" +
            "    \"created_at\": \"2014-03-28T17:55:38Z\",\n" +
            "    \"updated_at\": \"2016-04-26T12:19:44Z\",\n" +
            "    \"pushed_at\": \"2015-10-28T23:30:54Z\",\n" +
            "    \"git_url\": \"git://github.com/octocat/git-consortium.git\",\n" +
            "    \"ssh_url\": \"git@github.com:octocat/git-consortium.git\",\n" +
            "    \"clone_url\": \"https://github.com/octocat/git-consortium.git\",\n" +
            "    \"svn_url\": \"https://github.com/octocat/git-consortium\",\n" +
            "    \"homepage\": null,\n" +
            "    \"size\": 190,\n" +
            "    \"stargazers_count\": 7,\n" +
            "    \"watchers_count\": 7,\n" +
            "    \"language\": null,\n" +
            "    \"has_issues\": true,\n" +
            "    \"has_downloads\": true,\n" +
            "    \"has_wiki\": true,\n" +
            "    \"has_pages\": false,\n" +
            "    \"forks_count\": 15,\n" +
            "    \"mirror_url\": null,\n" +
            "    \"open_issues_count\": 3,\n" +
            "    \"forks\": 15,\n" +
            "    \"open_issues\": 3,\n" +
            "    \"watchers\": 7,\n" +
            "    \"default_branch\": \"master\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": 20978623,\n" +
            "    \"name\": \"hello-worId\",\n" +
            "    \"full_name\": \"octocat/hello-worId\",\n" +
            "    \"owner\": {\n" +
            "      \"login\": \"octocat\",\n" +
            "      \"id\": 583231,\n" +
            "      \"avatar_url\": \"https://avatars.githubusercontent.com/u/583231?v=3\",\n" +
            "      \"gravatar_id\": \"\",\n" +
            "      \"url\": \"https://api.github.com/users/octocat\",\n" +
            "      \"html_url\": \"https://github.com/octocat\",\n" +
            "      \"followers_url\": \"https://api.github.com/users/octocat/followers\",\n" +
            "      \"following_url\": \"https://api.github.com/users/octocat/following{/other_user}\",\n" +
            "      \"gists_url\": \"https://api.github.com/users/octocat/gists{/gist_id}\",\n" +
            "      \"starred_url\": \"https://api.github.com/users/octocat/starred{/owner}{/repo}\",\n" +
            "      \"subscriptions_url\": \"https://api.github.com/users/octocat/subscriptions\",\n" +
            "      \"organizations_url\": \"https://api.github.com/users/octocat/orgs\",\n" +
            "      \"repos_url\": \"https://api.github.com/users/octocat/repos\",\n" +
            "      \"events_url\": \"https://api.github.com/users/octocat/events{/privacy}\",\n" +
            "      \"received_events_url\": \"https://api.github.com/users/octocat/received_events\",\n" +
            "      \"type\": \"User\",\n" +
            "      \"site_admin\": false\n" +
            "    },\n" +
            "    \"private\": false,\n" +
            "    \"html_url\": \"https://github.com/octocat/hello-worId\",\n" +
            "    \"description\": \"My first repository on GitHub.\",\n" +
            "    \"fork\": false,\n" +
            "    \"url\": \"https://api.github.com/repos/octocat/hello-worId\",\n" +
            "    \"forks_url\": \"https://api.github.com/repos/octocat/hello-worId/forks\",\n" +
            "    \"keys_url\": \"https://api.github.com/repos/octocat/hello-worId/keys{/key_id}\",\n" +
            "    \"collaborators_url\": \"https://api.github.com/repos/octocat/hello-worId/collaborators{/collaborator}\",\n" +
            "    \"teams_url\": \"https://api.github.com/repos/octocat/hello-worId/teams\",\n" +
            "    \"hooks_url\": \"https://api.github.com/repos/octocat/hello-worId/hooks\",\n" +
            "    \"issue_events_url\": \"https://api.github.com/repos/octocat/hello-worId/issues/events{/number}\",\n" +
            "    \"events_url\": \"https://api.github.com/repos/octocat/hello-worId/events\",\n" +
            "    \"assignees_url\": \"https://api.github.com/repos/octocat/hello-worId/assignees{/user}\",\n" +
            "    \"branches_url\": \"https://api.github.com/repos/octocat/hello-worId/branches{/branch}\",\n" +
            "    \"tags_url\": \"https://api.github.com/repos/octocat/hello-worId/tags\",\n" +
            "    \"blobs_url\": \"https://api.github.com/repos/octocat/hello-worId/git/blobs{/sha}\",\n" +
            "    \"git_tags_url\": \"https://api.github.com/repos/octocat/hello-worId/git/tags{/sha}\",\n" +
            "    \"git_refs_url\": \"https://api.github.com/repos/octocat/hello-worId/git/refs{/sha}\",\n" +
            "    \"trees_url\": \"https://api.github.com/repos/octocat/hello-worId/git/trees{/sha}\",\n" +
            "    \"statuses_url\": \"https://api.github.com/repos/octocat/hello-worId/statuses/{sha}\",\n" +
            "    \"languages_url\": \"https://api.github.com/repos/octocat/hello-worId/languages\",\n" +
            "    \"stargazers_url\": \"https://api.github.com/repos/octocat/hello-worId/stargazers\",\n" +
            "    \"contributors_url\": \"https://api.github.com/repos/octocat/hello-worId/contributors\",\n" +
            "    \"subscribers_url\": \"https://api.github.com/repos/octocat/hello-worId/subscribers\",\n" +
            "    \"subscription_url\": \"https://api.github.com/repos/octocat/hello-worId/subscription\",\n" +
            "    \"commits_url\": \"https://api.github.com/repos/octocat/hello-worId/commits{/sha}\",\n" +
            "    \"git_commits_url\": \"https://api.github.com/repos/octocat/hello-worId/git/commits{/sha}\",\n" +
            "    \"comments_url\": \"https://api.github.com/repos/octocat/hello-worId/comments{/number}\",\n" +
            "    \"issue_comment_url\": \"https://api.github.com/repos/octocat/hello-worId/issues/comments{/number}\",\n" +
            "    \"contents_url\": \"https://api.github.com/repos/octocat/hello-worId/contents/{+path}\",\n" +
            "    \"compare_url\": \"https://api.github.com/repos/octocat/hello-worId/compare/{base}...{head}\",\n" +
            "    \"merges_url\": \"https://api.github.com/repos/octocat/hello-worId/merges\",\n" +
            "    \"archive_url\": \"https://api.github.com/repos/octocat/hello-worId/{archive_format}{/ref}\",\n" +
            "    \"downloads_url\": \"https://api.github.com/repos/octocat/hello-worId/downloads\",\n" +
            "    \"issues_url\": \"https://api.github.com/repos/octocat/hello-worId/issues{/number}\",\n" +
            "    \"pulls_url\": \"https://api.github.com/repos/octocat/hello-worId/pulls{/number}\",\n" +
            "    \"milestones_url\": \"https://api.github.com/repos/octocat/hello-worId/milestones{/number}\",\n" +
            "    \"notifications_url\": \"https://api.github.com/repos/octocat/hello-worId/notifications{?since,all,participating}\",\n" +
            "    \"labels_url\": \"https://api.github.com/repos/octocat/hello-worId/labels{/name}\",\n" +
            "    \"releases_url\": \"https://api.github.com/repos/octocat/hello-worId/releases{/id}\",\n" +
            "    \"deployments_url\": \"https://api.github.com/repos/octocat/hello-worId/deployments\",\n" +
            "    \"created_at\": \"2014-06-18T21:26:19Z\",\n" +
            "    \"updated_at\": \"2016-03-03T21:35:27Z\",\n" +
            "    \"pushed_at\": \"2015-06-17T09:48:41Z\",\n" +
            "    \"git_url\": \"git://github.com/octocat/hello-worId.git\",\n" +
            "    \"ssh_url\": \"git@github.com:octocat/hello-worId.git\",\n" +
            "    \"clone_url\": \"https://github.com/octocat/hello-worId.git\",\n" +
            "    \"svn_url\": \"https://github.com/octocat/hello-worId\",\n" +
            "    \"homepage\": null,\n" +
            "    \"size\": 160,\n" +
            "    \"stargazers_count\": 13,\n" +
            "    \"watchers_count\": 13,\n" +
            "    \"language\": null,\n" +
            "    \"has_issues\": true,\n" +
            "    \"has_downloads\": true,\n" +
            "    \"has_wiki\": true,\n" +
            "    \"has_pages\": false,\n" +
            "    \"forks_count\": 27,\n" +
            "    \"mirror_url\": null,\n" +
            "    \"open_issues_count\": 1,\n" +
            "    \"forks\": 27,\n" +
            "    \"open_issues\": 1,\n" +
            "    \"watchers\": 13,\n" +
            "    \"default_branch\": \"master\"\n" +
            "  }\n" +
            "]";

    @Rule
    public ActivityTestRule<RepositoriesListActivity> mainActivityRule = new ActivityTestRule(RepositoriesListActivity.class, true, false);

    /**
     * Reference:
     * https://github.com/googlecodelabs/android-testing/blob/master/app/src/androidTestMock/java/com/example/android/testing/notes/notedetail/NoteDetailScreenTest.java
     */

    @Before
    public void run_before_on_create() {

        Type listType = new TypeToken<ArrayList<RepositoryResponse>>() {
        }.getType();
        List<RepositoryResponse> repoList = new Gson().fromJson(OCTOCAT_REPO_MOCK, listType);

        RepositoryWrapper wrapper = new RepositoryWrapper();
        wrapper.setUsername("octocat");
        wrapper.setRepositories(repoList);

        Intent startIntent = new Intent();
        startIntent.putExtra(Util.PARCELABLE, wrapper);
        mainActivityRule.launchActivity(startIntent);

        registerIdlingResources();
    }

    @Test
    public void test_click_in_repo() throws InterruptedException {

        onView(withId(R.id.repositoriesRecyclerView)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        Thread.sleep(1500);
        Espresso.pressBack();
    }
}
