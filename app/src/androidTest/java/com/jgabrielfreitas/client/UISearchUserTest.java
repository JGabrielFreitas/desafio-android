package com.jgabrielfreitas.client;

import android.support.test.espresso.Espresso;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.jgabrielfreitas.client.activities.SearchUserActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by JGabrielFreitas on 21/05/16.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class UISearchUserTest {

    @Rule
    public ActivityTestRule<SearchUserActivity> mainActivityRule = new ActivityTestRule(SearchUserActivity.class);

    @Test
    public void test_search_user() {

        onView(withId(R.id.userToSearchEditText)).perform(typeText("octocat"));

        Espresso.closeSoftKeyboard();
        onView(withId(R.id.searchButton)).perform(click());

    }
}
