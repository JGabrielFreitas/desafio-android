package com.jgabrielfreitas.client.listeners;

import com.jgabrielfreitas.githubclient.core.json.github.RepositoryResponse;

/**
 * Created by JGabrielFreitas on 20/05/16.
 */
public interface OnRepoClickListener {

    void onRepoClick(RepositoryResponse repository);
}
