package com.jgabrielfreitas.client.util;

/**
 * Created by JGabrielFreitas on 22/05/16.
 */
public class ResponseCodes {

    public static final int SUCCESS = 200;
    public static final int NOT_FOUND = 404;

}
