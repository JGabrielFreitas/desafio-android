package com.jgabrielfreitas.client.wrappers;

import android.os.Parcel;
import android.os.Parcelable;

import com.jgabrielfreitas.githubclient.core.json.github.PullRequestResponse;

import java.util.List;

/**
 * Created by JGabrielFreitas on 20/05/16.
 */
public class PullRequestWrapper implements Parcelable {

    private List<PullRequestResponse> pullRequests;

    public List<PullRequestResponse> getPullRequests() {
        return pullRequests;
    }

    public void setPullRequests(List<PullRequestResponse> pullRequests) {
        this.pullRequests = pullRequests;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(pullRequests);
    }

    public PullRequestWrapper() {
    }

    protected PullRequestWrapper(Parcel in) {
        this.pullRequests = in.createTypedArrayList(PullRequestResponse.CREATOR);
    }

    public static final Parcelable.Creator<PullRequestWrapper> CREATOR = new Parcelable.Creator<PullRequestWrapper>() {
        @Override
        public PullRequestWrapper createFromParcel(Parcel source) {
            return new PullRequestWrapper(source);
        }

        @Override
        public PullRequestWrapper[] newArray(int size) {
            return new PullRequestWrapper[size];
        }
    };
}
