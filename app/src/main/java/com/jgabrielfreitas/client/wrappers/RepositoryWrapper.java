package com.jgabrielfreitas.client.wrappers;

import android.os.Parcel;
import android.os.Parcelable;

import com.jgabrielfreitas.githubclient.core.json.github.RepositoryResponse;

import java.util.List;

/**
 * Created by JGabrielFreitas on 19/05/16.
 */
public class RepositoryWrapper implements Parcelable {

    private String username;
    private List<RepositoryResponse> repositories;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<RepositoryResponse> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<RepositoryResponse> repositories) {
        this.repositories = repositories;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.username);
        dest.writeTypedList(repositories);
    }

    public RepositoryWrapper() {
    }

    protected RepositoryWrapper(Parcel in) {
        this.username = in.readString();
        this.repositories = in.createTypedArrayList(RepositoryResponse.CREATOR);
    }

    public static final Creator<RepositoryWrapper> CREATOR = new Creator<RepositoryWrapper>() {
        @Override
        public RepositoryWrapper createFromParcel(Parcel source) {
            return new RepositoryWrapper(source);
        }

        @Override
        public RepositoryWrapper[] newArray(int size) {
            return new RepositoryWrapper[size];
        }
    };
}
