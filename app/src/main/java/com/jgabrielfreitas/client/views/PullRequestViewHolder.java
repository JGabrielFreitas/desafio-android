package com.jgabrielfreitas.client.views;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jgabrielfreitas.client.R;
import com.jgabrielfreitas.githubclient.core.json.github.PullRequestResponse;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by JGabrielFreitas on 20/05/16.
 */
@LayoutId(R.layout.row_pull_request)
public class PullRequestViewHolder extends ItemViewHolder<PullRequestResponse> {

    @ViewId(R.id.repoNameTextView)
    TextView repoNameTextView;
    @ViewId(R.id.userNameTextView)
    TextView userNameTextView;
    @ViewId(R.id.pullRequestDescriptionTextView)
    TextView pullRequestDescriptionTextView;
    @ViewId(R.id.avatarImageView)
    CircleImageView avatarImageView;

    public PullRequestViewHolder(View view) {
        super(view);
    }

    public void onSetValues(PullRequestResponse item, PositionInfo positionInfo) {

        Glide.with(getContext()).load(getItem().getUser().getAvatarUrl()).dontAnimate().into(avatarImageView);
        pullRequestDescriptionTextView.setText(getItem().getBody());
        repoNameTextView.setText(getItem().getHead().getRepo().getName());
        userNameTextView.setText(getItem().getUser().getLogin());

        getView().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getItem().getHtmlUrl()));
                getContext().startActivity(browserIntent);
            }
        });
    }
}
