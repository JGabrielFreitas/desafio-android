package com.jgabrielfreitas.client.views;

import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jgabrielfreitas.client.R;
import com.jgabrielfreitas.client.listeners.OnRepoClickListener;
import com.jgabrielfreitas.githubclient.core.json.github.RepositoryResponse;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by JGabrielFreitas on 19/05/16.
 */
@LayoutId(R.layout.row_repo_card)
public class RepoViewHolder extends ItemViewHolder<RepositoryResponse> {

    @ViewId(R.id.repoNameTextView)
    TextView repoNameTextView;
    @ViewId(R.id.descriptionTextView)
    TextView descriptionTextView;
    @ViewId(R.id.forkCountTextView)
    TextView forkCountTextView;
    @ViewId(R.id.startCountTextView)
    TextView startCountTextView;
    @ViewId(R.id.avatarImageView)
    CircleImageView avatarImageView;

    public RepoViewHolder(View view) {
        super(view);
    }

    public void onSetValues(RepositoryResponse item, PositionInfo positionInfo) {

        Glide.with(getContext()).load(getItem().getOwner().getAvatarUrl()).dontAnimate().into(avatarImageView);

        repoNameTextView.setText(getItem().getName());
        descriptionTextView.setText(getItem().getDescription());
        forkCountTextView.setText("" + getItem().getForksCount());
        startCountTextView.setText("" + getItem().getStargazersCount());

        getView().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                OnRepoClickListener listener = getListener(OnRepoClickListener.class);
                if (listener != null)
                    listener.onRepoClick(getItem());
            }
        });

    }
}
