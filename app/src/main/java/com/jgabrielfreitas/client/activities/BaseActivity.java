package com.jgabrielfreitas.client.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.jgabrielfreitas.client.util.Util;
import com.jgabrielfreitas.githubclient.core.activities.CoreBaseActivity;

import butterknife.ButterKnife;

/**
 * Created by JGabrielFreitas on 18/05/16.
 */
public abstract class BaseActivity extends CoreBaseActivity {

    private Parcelable parcelableObject;
    private boolean isButterKnifeBinded = false;
    protected boolean killAfterIntent   = true;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().getExtras() != null)
            parcelableObject = getIntent().getExtras().getParcelable(Util.PARCELABLE);
    }

    protected void onStart() {
        super.onStart();

        bindButterKnife();
        modifyViews();
    }

    protected void modifyViews() {}

    protected void bindButterKnife() {
        if(!isButterKnifeBinded) {
            ButterKnife.bind(this);
            isButterKnifeBinded = true;
        }
    }

    protected void unbindButterKnife() {
        if(isButterKnifeBinded) {
            ButterKnife.unbind(this);
            isButterKnifeBinded = false;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        bindButterKnife();
    }

    protected void onStop() {
        super.onStop();

        unbindButterKnife();
    }

    /** Intent to some activity */
    public void doIntent(Class<?> classToIntent) {

        intentAndKill(classToIntent, null, 0);
    }

    /** Intent to some activity with any parcelable object */
    public void doIntent(Class<?> classToIntent, Parcelable parcelable) {

        intentAndKill(classToIntent, parcelable, 0);
    }

    public void doIntentWithRequest(Class<?> classToIntent, Parcelable parcelable, int requestCode) {

        intentAndKill(classToIntent, parcelable, requestCode);
    }

    private void intentAndKill(Class<?> classToIntent, Parcelable parcelable, int requestCode) {

        closeKeyboard();

        Intent intent = new Intent(this, classToIntent);

        if (parcelable != null)
            intent.putExtra(Util.PARCELABLE, parcelable);

        if (requestCode == 0)
            startActivity(intent);
        else
            startActivityForResult(intent, requestCode);

        if (killAfterIntent)
            killThisActivity();
    }

    /** just kill activity */
    protected void killThisActivity() {
        finish();
    }

    /** just close keyboard */
    public void closeKeyboard() {
        if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public Parcelable getParcelableObject() {
        return parcelableObject;
    }

    protected void configToolbarBackButton(Toolbar toolbar) {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    protected void log(String message) {
        Log.e(getClass().getSimpleName(), message);
    }

}
