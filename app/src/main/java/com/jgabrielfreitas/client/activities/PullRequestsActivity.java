package com.jgabrielfreitas.client.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.jgabrielfreitas.client.R;
import com.jgabrielfreitas.client.views.PullRequestViewHolder;
import com.jgabrielfreitas.client.wrappers.PullRequestWrapper;

import butterknife.Bind;
import uk.co.ribot.easyadapter.EasyRecyclerAdapter;

public class PullRequestsActivity extends BaseActivity {


    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.pullRequestsRecyclerView)
    RecyclerView pullRequestsRecyclerView;

    PullRequestWrapper wrapper;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests);

        wrapper = (PullRequestWrapper) getParcelableObject();
    }

    protected void modifyViews() {
        super.modifyViews();

        // just set repo name in Toolbar
        toolbar.setTitle(wrapper.getPullRequests().get(0).getHead().getRepo().getName());
        configToolbarBackButton(toolbar);

        pullRequestsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        pullRequestsRecyclerView.setAdapter(new EasyRecyclerAdapter<>(this, PullRequestViewHolder.class, wrapper.getPullRequests()));
    }
}
