package com.jgabrielfreitas.client.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.jgabrielfreitas.client.R;
import com.jgabrielfreitas.client.listeners.EndlessRecyclerOnScrollListener;
import com.jgabrielfreitas.client.listeners.OnRepoClickListener;
import com.jgabrielfreitas.client.views.RepoViewHolder;
import com.jgabrielfreitas.client.wrappers.PullRequestWrapper;
import com.jgabrielfreitas.client.wrappers.RepositoryWrapper;
import com.jgabrielfreitas.githubclient.core.activities.DialogUtils;
import com.jgabrielfreitas.githubclient.core.client.RxGithubClient;
import com.jgabrielfreitas.githubclient.core.json.github.PullRequestResponse;
import com.jgabrielfreitas.githubclient.core.json.github.RepositoryResponse;
import com.jgabrielfreitas.githubclient.core.reactive.RetrofitSubscriber;

import java.util.List;

import butterknife.Bind;
import retrofit2.Response;
import uk.co.ribot.easyadapter.EasyRecyclerAdapter;

public class RepositoriesListActivity extends BaseActivity implements OnRepoClickListener {

    List<RepositoryResponse> repositories;
    RepositoryWrapper wrapper;
    @Bind(R.id.repositoriesRecyclerView)
    RecyclerView repositoriesRecyclerView;
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    int INITIAL_PAGE = 1;
    int page = 2; // starts in 2 because the page 1 was passed by intent in SearchUserActivity

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repositories_list);
        killAfterIntent = false;

        // get all repositories
        wrapper = (RepositoryWrapper) getParcelableObject();
        repositories = wrapper.getRepositories();
    }

    protected void modifyViews() {
        super.modifyViews();

        toolbar.setTitle(wrapper.getUsername());
        configToolbarBackButton(toolbar);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        repositoriesRecyclerView.setLayoutManager(linearLayoutManager);
        repositoriesRecyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            public void onLoadMore(int current_page) {
                loadMoreRepo();
            }
        });

        repositoriesRecyclerView.setAdapter(new EasyRecyclerAdapter<>(this, RepoViewHolder.class, repositories, this));
    }

    // load repositories
    private void loadMoreRepo() {

        RxGithubClient rxGithubClient = new RxGithubClient(this);
        rxGithubClient.getRepositoryList(wrapper.getUsername(), page).subscribe(new RetrofitSubscriber<Response<List<RepositoryResponse>>>(this, true) {

            public void onNext(Response<List<RepositoryResponse>> gitHubRepositoryResponse) {
                super.onNext(gitHubRepositoryResponse);

                if (gitHubRepositoryResponse.body().size() > 0) {

                    addItemsAtList(gitHubRepositoryResponse.body());
                    page++; // next page
                } else {
                    page = INITIAL_PAGE; // restart pages to infinite scroll continue
                    loadMoreRepo();      // reload first page
                }
            }

            public void onError(Throwable t) {
                super.onError(t);
                t.printStackTrace();
            }
        });
    }

    // load pull requests
    public void onRepoClick(RepositoryResponse repository) {

        RxGithubClient rxGithubClient = new RxGithubClient(this);
        rxGithubClient.setExecuteFeedback(true);
        rxGithubClient.getPullRequests(wrapper.getUsername(), repository.getName()).subscribe(new RetrofitSubscriber<List<PullRequestResponse>>(this) {

            public void onNext(List<PullRequestResponse> pullRequestListResponses) {
                super.onNext(pullRequestListResponses);

                if (pullRequestListResponses.size() <= 0) {
                    DialogUtils.createDialogWithMessage(RepositoriesListActivity.this, getString(R.string.no_pull_request_found));
                } else {

                    PullRequestWrapper pullRequests = new PullRequestWrapper();
                    pullRequests.setPullRequests(pullRequestListResponses);

                    doIntent(PullRequestsActivity.class, pullRequests);
                }

            }
        });
    }

    // add items at recycler view
    private void addItemsAtList(List<RepositoryResponse> list) {

        // if user back when recycler view is refreshing,
        // this try catch hold application and don't crash
        try {

            // add new items and update UI
            ((EasyRecyclerAdapter<RepositoryResponse>) repositoriesRecyclerView.getAdapter()).addItems(list);
            repositoriesRecyclerView.getAdapter().notifyDataSetChanged();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
