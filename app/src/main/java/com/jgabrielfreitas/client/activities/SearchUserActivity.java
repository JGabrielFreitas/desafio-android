package com.jgabrielfreitas.client.activities;

import android.os.Bundle;
import android.text.TextUtils;

import com.jgabrielfreitas.client.R;
import com.jgabrielfreitas.client.util.ResponseCodes;
import com.jgabrielfreitas.client.wrappers.RepositoryWrapper;
import com.jgabrielfreitas.githubclient.core.activities.DialogUtils;
import com.jgabrielfreitas.githubclient.core.client.RxGithubClient;
import com.jgabrielfreitas.githubclient.core.json.github.RepositoryResponse;
import com.jgabrielfreitas.githubclient.core.reactive.RetrofitSubscriber;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Response;

public class SearchUserActivity extends BaseActivity {

    @Bind(R.id.userToSearchEditText) MaterialAutoCompleteTextView userToSearchEditText;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_search_user);
        killAfterIntent = false;
    }

    @OnClick(R.id.searchButton)
    public void searchUserRepos() {

        final String username = userToSearchEditText.getText().toString();

        if (TextUtils.isEmpty(username) == true) {
            DialogUtils.createDialogWithMessage(this, getString(R.string.invalid_field));
            return;
        }

        RxGithubClient rxGithubClient = new RxGithubClient(this);
        rxGithubClient.setExecuteFeedback(true);
        rxGithubClient.getRepositoryList(username, 0).subscribe(new RetrofitSubscriber<Response<List<RepositoryResponse>>>(this, true) {

            public void onNext(Response<List<RepositoryResponse>> gitHubRepositoryResponse) {
                super.onNext(gitHubRepositoryResponse);

                if (gitHubRepositoryResponse.code() == ResponseCodes.SUCCESS) {

                    if (gitHubRepositoryResponse.body().size() > 0) {

                        RepositoryWrapper repositoryWrapper = new RepositoryWrapper();
                        repositoryWrapper.setUsername(username);
                        repositoryWrapper.setRepositories(gitHubRepositoryResponse.body());

                        doIntent(RepositoriesListActivity.class, repositoryWrapper);

                    } else
                        DialogUtils.createDialogWithMessage(SearchUserActivity.this, getString(R.string.no_repo_found));
                } else if (gitHubRepositoryResponse.code() == ResponseCodes.NOT_FOUND) {
                    DialogUtils.createDialogWithMessage(SearchUserActivity.this, getString(R.string.user_not_found));
                }
            }

            public void onError(Throwable t) {
                super.onError(t);
                t.printStackTrace();
            }
        });

    }
}
