package com.jgabrielfreitas.client;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jgabrielfreitas.githubclient.core.json.github.PullRequestResponse;
import com.jgabrielfreitas.githubclient.core.json.github.RepositoryResponse;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by JGabrielFreitas on 23/05/16.
 */
public class DeserializeTest {

    @Test
    public void deserialize_repo_list() {

        String OCTOCAT_REPO_MOCK = "[\n" +
                "  {\n" +
                "    \"id\": 18221276,\n" +
                "    \"name\": \"git-consortium\",\n" +
                "    \"full_name\": \"octocat/git-consortium\",\n" +
                "    \"owner\": {\n" +
                "      \"login\": \"octocat\",\n" +
                "      \"id\": 583231,\n" +
                "      \"avatar_url\": \"https://avatars.githubusercontent.com/u/583231?v=3\",\n" +
                "      \"gravatar_id\": \"\",\n" +
                "      \"url\": \"https://api.github.com/users/octocat\",\n" +
                "      \"html_url\": \"https://github.com/octocat\",\n" +
                "      \"followers_url\": \"https://api.github.com/users/octocat/followers\",\n" +
                "      \"following_url\": \"https://api.github.com/users/octocat/following{/other_user}\",\n" +
                "      \"gists_url\": \"https://api.github.com/users/octocat/gists{/gist_id}\",\n" +
                "      \"starred_url\": \"https://api.github.com/users/octocat/starred{/owner}{/repo}\",\n" +
                "      \"subscriptions_url\": \"https://api.github.com/users/octocat/subscriptions\",\n" +
                "      \"organizations_url\": \"https://api.github.com/users/octocat/orgs\",\n" +
                "      \"repos_url\": \"https://api.github.com/users/octocat/repos\",\n" +
                "      \"events_url\": \"https://api.github.com/users/octocat/events{/privacy}\",\n" +
                "      \"received_events_url\": \"https://api.github.com/users/octocat/received_events\",\n" +
                "      \"type\": \"User\",\n" +
                "      \"site_admin\": false\n" +
                "    },\n" +
                "    \"private\": false,\n" +
                "    \"html_url\": \"https://github.com/octocat/git-consortium\",\n" +
                "    \"description\": \"This repo is for demonstration purposes only.\",\n" +
                "    \"fork\": false,\n" +
                "    \"url\": \"https://api.github.com/repos/octocat/git-consortium\",\n" +
                "    \"forks_url\": \"https://api.github.com/repos/octocat/git-consortium/forks\",\n" +
                "    \"keys_url\": \"https://api.github.com/repos/octocat/git-consortium/keys{/key_id}\",\n" +
                "    \"collaborators_url\": \"https://api.github.com/repos/octocat/git-consortium/collaborators{/collaborator}\",\n" +
                "    \"teams_url\": \"https://api.github.com/repos/octocat/git-consortium/teams\",\n" +
                "    \"hooks_url\": \"https://api.github.com/repos/octocat/git-consortium/hooks\",\n" +
                "    \"issue_events_url\": \"https://api.github.com/repos/octocat/git-consortium/issues/events{/number}\",\n" +
                "    \"events_url\": \"https://api.github.com/repos/octocat/git-consortium/events\",\n" +
                "    \"assignees_url\": \"https://api.github.com/repos/octocat/git-consortium/assignees{/user}\",\n" +
                "    \"branches_url\": \"https://api.github.com/repos/octocat/git-consortium/branches{/branch}\",\n" +
                "    \"tags_url\": \"https://api.github.com/repos/octocat/git-consortium/tags\",\n" +
                "    \"blobs_url\": \"https://api.github.com/repos/octocat/git-consortium/git/blobs{/sha}\",\n" +
                "    \"git_tags_url\": \"https://api.github.com/repos/octocat/git-consortium/git/tags{/sha}\",\n" +
                "    \"git_refs_url\": \"https://api.github.com/repos/octocat/git-consortium/git/refs{/sha}\",\n" +
                "    \"trees_url\": \"https://api.github.com/repos/octocat/git-consortium/git/trees{/sha}\",\n" +
                "    \"statuses_url\": \"https://api.github.com/repos/octocat/git-consortium/statuses/{sha}\",\n" +
                "    \"languages_url\": \"https://api.github.com/repos/octocat/git-consortium/languages\",\n" +
                "    \"stargazers_url\": \"https://api.github.com/repos/octocat/git-consortium/stargazers\",\n" +
                "    \"contributors_url\": \"https://api.github.com/repos/octocat/git-consortium/contributors\",\n" +
                "    \"subscribers_url\": \"https://api.github.com/repos/octocat/git-consortium/subscribers\",\n" +
                "    \"subscription_url\": \"https://api.github.com/repos/octocat/git-consortium/subscription\",\n" +
                "    \"commits_url\": \"https://api.github.com/repos/octocat/git-consortium/commits{/sha}\",\n" +
                "    \"git_commits_url\": \"https://api.github.com/repos/octocat/git-consortium/git/commits{/sha}\",\n" +
                "    \"comments_url\": \"https://api.github.com/repos/octocat/git-consortium/comments{/number}\",\n" +
                "    \"issue_comment_url\": \"https://api.github.com/repos/octocat/git-consortium/issues/comments{/number}\",\n" +
                "    \"contents_url\": \"https://api.github.com/repos/octocat/git-consortium/contents/{+path}\",\n" +
                "    \"compare_url\": \"https://api.github.com/repos/octocat/git-consortium/compare/{base}...{head}\",\n" +
                "    \"merges_url\": \"https://api.github.com/repos/octocat/git-consortium/merges\",\n" +
                "    \"archive_url\": \"https://api.github.com/repos/octocat/git-consortium/{archive_format}{/ref}\",\n" +
                "    \"downloads_url\": \"https://api.github.com/repos/octocat/git-consortium/downloads\",\n" +
                "    \"issues_url\": \"https://api.github.com/repos/octocat/git-consortium/issues{/number}\",\n" +
                "    \"pulls_url\": \"https://api.github.com/repos/octocat/git-consortium/pulls{/number}\",\n" +
                "    \"milestones_url\": \"https://api.github.com/repos/octocat/git-consortium/milestones{/number}\",\n" +
                "    \"notifications_url\": \"https://api.github.com/repos/octocat/git-consortium/notifications{?since,all,participating}\",\n" +
                "    \"labels_url\": \"https://api.github.com/repos/octocat/git-consortium/labels{/name}\",\n" +
                "    \"releases_url\": \"https://api.github.com/repos/octocat/git-consortium/releases{/id}\",\n" +
                "    \"deployments_url\": \"https://api.github.com/repos/octocat/git-consortium/deployments\",\n" +
                "    \"created_at\": \"2014-03-28T17:55:38Z\",\n" +
                "    \"updated_at\": \"2016-04-26T12:19:44Z\",\n" +
                "    \"pushed_at\": \"2015-10-28T23:30:54Z\",\n" +
                "    \"git_url\": \"git://github.com/octocat/git-consortium.git\",\n" +
                "    \"ssh_url\": \"git@github.com:octocat/git-consortium.git\",\n" +
                "    \"clone_url\": \"https://github.com/octocat/git-consortium.git\",\n" +
                "    \"svn_url\": \"https://github.com/octocat/git-consortium\",\n" +
                "    \"homepage\": null,\n" +
                "    \"size\": 190,\n" +
                "    \"stargazers_count\": 7,\n" +
                "    \"watchers_count\": 7,\n" +
                "    \"language\": null,\n" +
                "    \"has_issues\": true,\n" +
                "    \"has_downloads\": true,\n" +
                "    \"has_wiki\": true,\n" +
                "    \"has_pages\": false,\n" +
                "    \"forks_count\": 15,\n" +
                "    \"mirror_url\": null,\n" +
                "    \"open_issues_count\": 3,\n" +
                "    \"forks\": 15,\n" +
                "    \"open_issues\": 3,\n" +
                "    \"watchers\": 7,\n" +
                "    \"default_branch\": \"master\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 20978623,\n" +
                "    \"name\": \"hello-worId\",\n" +
                "    \"full_name\": \"octocat/hello-worId\",\n" +
                "    \"owner\": {\n" +
                "      \"login\": \"octocat\",\n" +
                "      \"id\": 583231,\n" +
                "      \"avatar_url\": \"https://avatars.githubusercontent.com/u/583231?v=3\",\n" +
                "      \"gravatar_id\": \"\",\n" +
                "      \"url\": \"https://api.github.com/users/octocat\",\n" +
                "      \"html_url\": \"https://github.com/octocat\",\n" +
                "      \"followers_url\": \"https://api.github.com/users/octocat/followers\",\n" +
                "      \"following_url\": \"https://api.github.com/users/octocat/following{/other_user}\",\n" +
                "      \"gists_url\": \"https://api.github.com/users/octocat/gists{/gist_id}\",\n" +
                "      \"starred_url\": \"https://api.github.com/users/octocat/starred{/owner}{/repo}\",\n" +
                "      \"subscriptions_url\": \"https://api.github.com/users/octocat/subscriptions\",\n" +
                "      \"organizations_url\": \"https://api.github.com/users/octocat/orgs\",\n" +
                "      \"repos_url\": \"https://api.github.com/users/octocat/repos\",\n" +
                "      \"events_url\": \"https://api.github.com/users/octocat/events{/privacy}\",\n" +
                "      \"received_events_url\": \"https://api.github.com/users/octocat/received_events\",\n" +
                "      \"type\": \"User\",\n" +
                "      \"site_admin\": false\n" +
                "    },\n" +
                "    \"private\": false,\n" +
                "    \"html_url\": \"https://github.com/octocat/hello-worId\",\n" +
                "    \"description\": \"My first repository on GitHub.\",\n" +
                "    \"fork\": false,\n" +
                "    \"url\": \"https://api.github.com/repos/octocat/hello-worId\",\n" +
                "    \"forks_url\": \"https://api.github.com/repos/octocat/hello-worId/forks\",\n" +
                "    \"keys_url\": \"https://api.github.com/repos/octocat/hello-worId/keys{/key_id}\",\n" +
                "    \"collaborators_url\": \"https://api.github.com/repos/octocat/hello-worId/collaborators{/collaborator}\",\n" +
                "    \"teams_url\": \"https://api.github.com/repos/octocat/hello-worId/teams\",\n" +
                "    \"hooks_url\": \"https://api.github.com/repos/octocat/hello-worId/hooks\",\n" +
                "    \"issue_events_url\": \"https://api.github.com/repos/octocat/hello-worId/issues/events{/number}\",\n" +
                "    \"events_url\": \"https://api.github.com/repos/octocat/hello-worId/events\",\n" +
                "    \"assignees_url\": \"https://api.github.com/repos/octocat/hello-worId/assignees{/user}\",\n" +
                "    \"branches_url\": \"https://api.github.com/repos/octocat/hello-worId/branches{/branch}\",\n" +
                "    \"tags_url\": \"https://api.github.com/repos/octocat/hello-worId/tags\",\n" +
                "    \"blobs_url\": \"https://api.github.com/repos/octocat/hello-worId/git/blobs{/sha}\",\n" +
                "    \"git_tags_url\": \"https://api.github.com/repos/octocat/hello-worId/git/tags{/sha}\",\n" +
                "    \"git_refs_url\": \"https://api.github.com/repos/octocat/hello-worId/git/refs{/sha}\",\n" +
                "    \"trees_url\": \"https://api.github.com/repos/octocat/hello-worId/git/trees{/sha}\",\n" +
                "    \"statuses_url\": \"https://api.github.com/repos/octocat/hello-worId/statuses/{sha}\",\n" +
                "    \"languages_url\": \"https://api.github.com/repos/octocat/hello-worId/languages\",\n" +
                "    \"stargazers_url\": \"https://api.github.com/repos/octocat/hello-worId/stargazers\",\n" +
                "    \"contributors_url\": \"https://api.github.com/repos/octocat/hello-worId/contributors\",\n" +
                "    \"subscribers_url\": \"https://api.github.com/repos/octocat/hello-worId/subscribers\",\n" +
                "    \"subscription_url\": \"https://api.github.com/repos/octocat/hello-worId/subscription\",\n" +
                "    \"commits_url\": \"https://api.github.com/repos/octocat/hello-worId/commits{/sha}\",\n" +
                "    \"git_commits_url\": \"https://api.github.com/repos/octocat/hello-worId/git/commits{/sha}\",\n" +
                "    \"comments_url\": \"https://api.github.com/repos/octocat/hello-worId/comments{/number}\",\n" +
                "    \"issue_comment_url\": \"https://api.github.com/repos/octocat/hello-worId/issues/comments{/number}\",\n" +
                "    \"contents_url\": \"https://api.github.com/repos/octocat/hello-worId/contents/{+path}\",\n" +
                "    \"compare_url\": \"https://api.github.com/repos/octocat/hello-worId/compare/{base}...{head}\",\n" +
                "    \"merges_url\": \"https://api.github.com/repos/octocat/hello-worId/merges\",\n" +
                "    \"archive_url\": \"https://api.github.com/repos/octocat/hello-worId/{archive_format}{/ref}\",\n" +
                "    \"downloads_url\": \"https://api.github.com/repos/octocat/hello-worId/downloads\",\n" +
                "    \"issues_url\": \"https://api.github.com/repos/octocat/hello-worId/issues{/number}\",\n" +
                "    \"pulls_url\": \"https://api.github.com/repos/octocat/hello-worId/pulls{/number}\",\n" +
                "    \"milestones_url\": \"https://api.github.com/repos/octocat/hello-worId/milestones{/number}\",\n" +
                "    \"notifications_url\": \"https://api.github.com/repos/octocat/hello-worId/notifications{?since,all,participating}\",\n" +
                "    \"labels_url\": \"https://api.github.com/repos/octocat/hello-worId/labels{/name}\",\n" +
                "    \"releases_url\": \"https://api.github.com/repos/octocat/hello-worId/releases{/id}\",\n" +
                "    \"deployments_url\": \"https://api.github.com/repos/octocat/hello-worId/deployments\",\n" +
                "    \"created_at\": \"2014-06-18T21:26:19Z\",\n" +
                "    \"updated_at\": \"2016-03-03T21:35:27Z\",\n" +
                "    \"pushed_at\": \"2015-06-17T09:48:41Z\",\n" +
                "    \"git_url\": \"git://github.com/octocat/hello-worId.git\",\n" +
                "    \"ssh_url\": \"git@github.com:octocat/hello-worId.git\",\n" +
                "    \"clone_url\": \"https://github.com/octocat/hello-worId.git\",\n" +
                "    \"svn_url\": \"https://github.com/octocat/hello-worId\",\n" +
                "    \"homepage\": null,\n" +
                "    \"size\": 160,\n" +
                "    \"stargazers_count\": 13,\n" +
                "    \"watchers_count\": 13,\n" +
                "    \"language\": null,\n" +
                "    \"has_issues\": true,\n" +
                "    \"has_downloads\": true,\n" +
                "    \"has_wiki\": true,\n" +
                "    \"has_pages\": false,\n" +
                "    \"forks_count\": 27,\n" +
                "    \"mirror_url\": null,\n" +
                "    \"open_issues_count\": 1,\n" +
                "    \"forks\": 27,\n" +
                "    \"open_issues\": 1,\n" +
                "    \"watchers\": 13,\n" +
                "    \"default_branch\": \"master\"\n" +
                "  }\n" +
                "]";

        Type listType = new TypeToken<ArrayList<RepositoryResponse>>() {
        }.getType();
        List<RepositoryResponse> repoList = new Gson().fromJson(OCTOCAT_REPO_MOCK, listType);

        Assert.assertNotNull(repoList);

    }

    @Test
    public void deserialize_pull_request_list() {

        String PULL_REQUESTS_MOCK = "[\n" +
                "  {\n" +
                "    \"url\": \"https://api.github.com/repos/octocat/git-consortium/pulls/7\",\n" +
                "    \"id\": 49081547,\n" +
                "    \"html_url\": \"https://github.com/octocat/git-consortium/pull/7\",\n" +
                "    \"diff_url\": \"https://github.com/octocat/git-consortium/pull/7.diff\",\n" +
                "    \"patch_url\": \"https://github.com/octocat/git-consortium/pull/7.patch\",\n" +
                "    \"issue_url\": \"https://api.github.com/repos/octocat/git-consortium/issues/7\",\n" +
                "    \"number\": 7,\n" +
                "    \"state\": \"open\",\n" +
                "    \"locked\": false,\n" +
                "    \"title\": \"Update README.md\",\n" +
                "    \"user\": {\n" +
                "      \"login\": \"payzot\",\n" +
                "      \"id\": 15370882,\n" +
                "      \"avatar_url\": \"https://avatars.githubusercontent.com/u/15370882?v=3\",\n" +
                "      \"gravatar_id\": \"\",\n" +
                "      \"url\": \"https://api.github.com/users/payzot\",\n" +
                "      \"html_url\": \"https://github.com/payzot\",\n" +
                "      \"followers_url\": \"https://api.github.com/users/payzot/followers\",\n" +
                "      \"following_url\": \"https://api.github.com/users/payzot/following{/other_user}\",\n" +
                "      \"gists_url\": \"https://api.github.com/users/payzot/gists{/gist_id}\",\n" +
                "      \"starred_url\": \"https://api.github.com/users/payzot/starred{/owner}{/repo}\",\n" +
                "      \"subscriptions_url\": \"https://api.github.com/users/payzot/subscriptions\",\n" +
                "      \"organizations_url\": \"https://api.github.com/users/payzot/orgs\",\n" +
                "      \"repos_url\": \"https://api.github.com/users/payzot/repos\",\n" +
                "      \"events_url\": \"https://api.github.com/users/payzot/events{/privacy}\",\n" +
                "      \"received_events_url\": \"https://api.github.com/users/payzot/received_events\",\n" +
                "      \"type\": \"User\",\n" +
                "      \"site_admin\": false\n" +
                "    },\n" +
                "    \"body\": \"\",\n" +
                "    \"created_at\": \"2015-10-28T23:30:54Z\",\n" +
                "    \"updated_at\": \"2015-10-28T23:30:54Z\",\n" +
                "    \"closed_at\": null,\n" +
                "    \"merged_at\": null,\n" +
                "    \"merge_commit_sha\": \"61e88666388895c08c05954ae7cf6c44ed2440f6\",\n" +
                "    \"assignee\": null,\n" +
                "    \"milestone\": null,\n" +
                "    \"commits_url\": \"https://api.github.com/repos/octocat/git-consortium/pulls/7/commits\",\n" +
                "    \"review_comments_url\": \"https://api.github.com/repos/octocat/git-consortium/pulls/7/comments\",\n" +
                "    \"review_comment_url\": \"https://api.github.com/repos/octocat/git-consortium/pulls/comments{/number}\",\n" +
                "    \"comments_url\": \"https://api.github.com/repos/octocat/git-consortium/issues/7/comments\",\n" +
                "    \"statuses_url\": \"https://api.github.com/repos/octocat/git-consortium/statuses/a28eeec1a059e528f28bd066bc24cc0a0b32b92e\",\n" +
                "    \"head\": {\n" +
                "      \"label\": \"payzot:patch-1\",\n" +
                "      \"ref\": \"patch-1\",\n" +
                "      \"sha\": \"a28eeec1a059e528f28bd066bc24cc0a0b32b92e\",\n" +
                "      \"user\": {\n" +
                "        \"login\": \"payzot\",\n" +
                "        \"id\": 15370882,\n" +
                "        \"avatar_url\": \"https://avatars.githubusercontent.com/u/15370882?v=3\",\n" +
                "        \"gravatar_id\": \"\",\n" +
                "        \"url\": \"https://api.github.com/users/payzot\",\n" +
                "        \"html_url\": \"https://github.com/payzot\",\n" +
                "        \"followers_url\": \"https://api.github.com/users/payzot/followers\",\n" +
                "        \"following_url\": \"https://api.github.com/users/payzot/following{/other_user}\",\n" +
                "        \"gists_url\": \"https://api.github.com/users/payzot/gists{/gist_id}\",\n" +
                "        \"starred_url\": \"https://api.github.com/users/payzot/starred{/owner}{/repo}\",\n" +
                "        \"subscriptions_url\": \"https://api.github.com/users/payzot/subscriptions\",\n" +
                "        \"organizations_url\": \"https://api.github.com/users/payzot/orgs\",\n" +
                "        \"repos_url\": \"https://api.github.com/users/payzot/repos\",\n" +
                "        \"events_url\": \"https://api.github.com/users/payzot/events{/privacy}\",\n" +
                "        \"received_events_url\": \"https://api.github.com/users/payzot/received_events\",\n" +
                "        \"type\": \"User\",\n" +
                "        \"site_admin\": false\n" +
                "      },\n" +
                "      \"repo\": {\n" +
                "        \"id\": 45148048,\n" +
                "        \"name\": \"git-consortium\",\n" +
                "        \"full_name\": \"payzot/git-consortium\",\n" +
                "        \"owner\": {\n" +
                "          \"login\": \"payzot\",\n" +
                "          \"id\": 15370882,\n" +
                "          \"avatar_url\": \"https://avatars.githubusercontent.com/u/15370882?v=3\",\n" +
                "          \"gravatar_id\": \"\",\n" +
                "          \"url\": \"https://api.github.com/users/payzot\",\n" +
                "          \"html_url\": \"https://github.com/payzot\",\n" +
                "          \"followers_url\": \"https://api.github.com/users/payzot/followers\",\n" +
                "          \"following_url\": \"https://api.github.com/users/payzot/following{/other_user}\",\n" +
                "          \"gists_url\": \"https://api.github.com/users/payzot/gists{/gist_id}\",\n" +
                "          \"starred_url\": \"https://api.github.com/users/payzot/starred{/owner}{/repo}\",\n" +
                "          \"subscriptions_url\": \"https://api.github.com/users/payzot/subscriptions\",\n" +
                "          \"organizations_url\": \"https://api.github.com/users/payzot/orgs\",\n" +
                "          \"repos_url\": \"https://api.github.com/users/payzot/repos\",\n" +
                "          \"events_url\": \"https://api.github.com/users/payzot/events{/privacy}\",\n" +
                "          \"received_events_url\": \"https://api.github.com/users/payzot/received_events\",\n" +
                "          \"type\": \"User\",\n" +
                "          \"site_admin\": false\n" +
                "        },\n" +
                "        \"private\": false,\n" +
                "        \"html_url\": \"https://github.com/payzot/git-consortium\",\n" +
                "        \"description\": \"This repo is for demonstration purposes only.\",\n" +
                "        \"fork\": true,\n" +
                "        \"url\": \"https://api.github.com/repos/payzot/git-consortium\",\n" +
                "        \"forks_url\": \"https://api.github.com/repos/payzot/git-consortium/forks\",\n" +
                "        \"keys_url\": \"https://api.github.com/repos/payzot/git-consortium/keys{/key_id}\",\n" +
                "        \"collaborators_url\": \"https://api.github.com/repos/payzot/git-consortium/collaborators{/collaborator}\",\n" +
                "        \"teams_url\": \"https://api.github.com/repos/payzot/git-consortium/teams\",\n" +
                "        \"hooks_url\": \"https://api.github.com/repos/payzot/git-consortium/hooks\",\n" +
                "        \"issue_events_url\": \"https://api.github.com/repos/payzot/git-consortium/issues/events{/number}\",\n" +
                "        \"events_url\": \"https://api.github.com/repos/payzot/git-consortium/events\",\n" +
                "        \"assignees_url\": \"https://api.github.com/repos/payzot/git-consortium/assignees{/user}\",\n" +
                "        \"branches_url\": \"https://api.github.com/repos/payzot/git-consortium/branches{/branch}\",\n" +
                "        \"tags_url\": \"https://api.github.com/repos/payzot/git-consortium/tags\",\n" +
                "        \"blobs_url\": \"https://api.github.com/repos/payzot/git-consortium/git/blobs{/sha}\",\n" +
                "        \"git_tags_url\": \"https://api.github.com/repos/payzot/git-consortium/git/tags{/sha}\",\n" +
                "        \"git_refs_url\": \"https://api.github.com/repos/payzot/git-consortium/git/refs{/sha}\",\n" +
                "        \"trees_url\": \"https://api.github.com/repos/payzot/git-consortium/git/trees{/sha}\",\n" +
                "        \"statuses_url\": \"https://api.github.com/repos/payzot/git-consortium/statuses/{sha}\",\n" +
                "        \"languages_url\": \"https://api.github.com/repos/payzot/git-consortium/languages\",\n" +
                "        \"stargazers_url\": \"https://api.github.com/repos/payzot/git-consortium/stargazers\",\n" +
                "        \"contributors_url\": \"https://api.github.com/repos/payzot/git-consortium/contributors\",\n" +
                "        \"subscribers_url\": \"https://api.github.com/repos/payzot/git-consortium/subscribers\",\n" +
                "        \"subscription_url\": \"https://api.github.com/repos/payzot/git-consortium/subscription\",\n" +
                "        \"commits_url\": \"https://api.github.com/repos/payzot/git-consortium/commits{/sha}\",\n" +
                "        \"git_commits_url\": \"https://api.github.com/repos/payzot/git-consortium/git/commits{/sha}\",\n" +
                "        \"comments_url\": \"https://api.github.com/repos/payzot/git-consortium/comments{/number}\",\n" +
                "        \"issue_comment_url\": \"https://api.github.com/repos/payzot/git-consortium/issues/comments{/number}\",\n" +
                "        \"contents_url\": \"https://api.github.com/repos/payzot/git-consortium/contents/{+path}\",\n" +
                "        \"compare_url\": \"https://api.github.com/repos/payzot/git-consortium/compare/{base}...{head}\",\n" +
                "        \"merges_url\": \"https://api.github.com/repos/payzot/git-consortium/merges\",\n" +
                "        \"archive_url\": \"https://api.github.com/repos/payzot/git-consortium/{archive_format}{/ref}\",\n" +
                "        \"downloads_url\": \"https://api.github.com/repos/payzot/git-consortium/downloads\",\n" +
                "        \"issues_url\": \"https://api.github.com/repos/payzot/git-consortium/issues{/number}\",\n" +
                "        \"pulls_url\": \"https://api.github.com/repos/payzot/git-consortium/pulls{/number}\",\n" +
                "        \"milestones_url\": \"https://api.github.com/repos/payzot/git-consortium/milestones{/number}\",\n" +
                "        \"notifications_url\": \"https://api.github.com/repos/payzot/git-consortium/notifications{?since,all,participating}\",\n" +
                "        \"labels_url\": \"https://api.github.com/repos/payzot/git-consortium/labels{/name}\",\n" +
                "        \"releases_url\": \"https://api.github.com/repos/payzot/git-consortium/releases{/id}\",\n" +
                "        \"deployments_url\": \"https://api.github.com/repos/payzot/git-consortium/deployments\",\n" +
                "        \"created_at\": \"2015-10-28T23:30:34Z\",\n" +
                "        \"updated_at\": \"2015-10-12T22:15:23Z\",\n" +
                "        \"pushed_at\": \"2015-10-28T23:30:50Z\",\n" +
                "        \"git_url\": \"git://github.com/payzot/git-consortium.git\",\n" +
                "        \"ssh_url\": \"git@github.com:payzot/git-consortium.git\",\n" +
                "        \"clone_url\": \"https://github.com/payzot/git-consortium.git\",\n" +
                "        \"svn_url\": \"https://github.com/payzot/git-consortium\",\n" +
                "        \"homepage\": null,\n" +
                "        \"size\": 99,\n" +
                "        \"stargazers_count\": 0,\n" +
                "        \"watchers_count\": 0,\n" +
                "        \"language\": null,\n" +
                "        \"has_issues\": false,\n" +
                "        \"has_downloads\": true,\n" +
                "        \"has_wiki\": true,\n" +
                "        \"has_pages\": false,\n" +
                "        \"forks_count\": 0,\n" +
                "        \"mirror_url\": null,\n" +
                "        \"open_issues_count\": 0,\n" +
                "        \"forks\": 0,\n" +
                "        \"open_issues\": 0,\n" +
                "        \"watchers\": 0,\n" +
                "        \"default_branch\": \"master\"\n" +
                "      }\n" +
                "    },\n" +
                "    \"base\": {\n" +
                "      \"label\": \"octocat:master\",\n" +
                "      \"ref\": \"master\",\n" +
                "      \"sha\": \"b33a9c7c02ad93f621fa38f0e9fc9e867e12fa0e\",\n" +
                "      \"user\": {\n" +
                "        \"login\": \"octocat\",\n" +
                "        \"id\": 583231,\n" +
                "        \"avatar_url\": \"https://avatars.githubusercontent.com/u/583231?v=3\",\n" +
                "        \"gravatar_id\": \"\",\n" +
                "        \"url\": \"https://api.github.com/users/octocat\",\n" +
                "        \"html_url\": \"https://github.com/octocat\",\n" +
                "        \"followers_url\": \"https://api.github.com/users/octocat/followers\",\n" +
                "        \"following_url\": \"https://api.github.com/users/octocat/following{/other_user}\",\n" +
                "        \"gists_url\": \"https://api.github.com/users/octocat/gists{/gist_id}\",\n" +
                "        \"starred_url\": \"https://api.github.com/users/octocat/starred{/owner}{/repo}\",\n" +
                "        \"subscriptions_url\": \"https://api.github.com/users/octocat/subscriptions\",\n" +
                "        \"organizations_url\": \"https://api.github.com/users/octocat/orgs\",\n" +
                "        \"repos_url\": \"https://api.github.com/users/octocat/repos\",\n" +
                "        \"events_url\": \"https://api.github.com/users/octocat/events{/privacy}\",\n" +
                "        \"received_events_url\": \"https://api.github.com/users/octocat/received_events\",\n" +
                "        \"type\": \"User\",\n" +
                "        \"site_admin\": false\n" +
                "      },\n" +
                "      \"repo\": {\n" +
                "        \"id\": 18221276,\n" +
                "        \"name\": \"git-consortium\",\n" +
                "        \"full_name\": \"octocat/git-consortium\",\n" +
                "        \"owner\": {\n" +
                "          \"login\": \"octocat\",\n" +
                "          \"id\": 583231,\n" +
                "          \"avatar_url\": \"https://avatars.githubusercontent.com/u/583231?v=3\",\n" +
                "          \"gravatar_id\": \"\",\n" +
                "          \"url\": \"https://api.github.com/users/octocat\",\n" +
                "          \"html_url\": \"https://github.com/octocat\",\n" +
                "          \"followers_url\": \"https://api.github.com/users/octocat/followers\",\n" +
                "          \"following_url\": \"https://api.github.com/users/octocat/following{/other_user}\",\n" +
                "          \"gists_url\": \"https://api.github.com/users/octocat/gists{/gist_id}\",\n" +
                "          \"starred_url\": \"https://api.github.com/users/octocat/starred{/owner}{/repo}\",\n" +
                "          \"subscriptions_url\": \"https://api.github.com/users/octocat/subscriptions\",\n" +
                "          \"organizations_url\": \"https://api.github.com/users/octocat/orgs\",\n" +
                "          \"repos_url\": \"https://api.github.com/users/octocat/repos\",\n" +
                "          \"events_url\": \"https://api.github.com/users/octocat/events{/privacy}\",\n" +
                "          \"received_events_url\": \"https://api.github.com/users/octocat/received_events\",\n" +
                "          \"type\": \"User\",\n" +
                "          \"site_admin\": false\n" +
                "        },\n" +
                "        \"private\": false,\n" +
                "        \"html_url\": \"https://github.com/octocat/git-consortium\",\n" +
                "        \"description\": \"This repo is for demonstration purposes only.\",\n" +
                "        \"fork\": false,\n" +
                "        \"url\": \"https://api.github.com/repos/octocat/git-consortium\",\n" +
                "        \"forks_url\": \"https://api.github.com/repos/octocat/git-consortium/forks\",\n" +
                "        \"keys_url\": \"https://api.github.com/repos/octocat/git-consortium/keys{/key_id}\",\n" +
                "        \"collaborators_url\": \"https://api.github.com/repos/octocat/git-consortium/collaborators{/collaborator}\",\n" +
                "        \"teams_url\": \"https://api.github.com/repos/octocat/git-consortium/teams\",\n" +
                "        \"hooks_url\": \"https://api.github.com/repos/octocat/git-consortium/hooks\",\n" +
                "        \"issue_events_url\": \"https://api.github.com/repos/octocat/git-consortium/issues/events{/number}\",\n" +
                "        \"events_url\": \"https://api.github.com/repos/octocat/git-consortium/events\",\n" +
                "        \"assignees_url\": \"https://api.github.com/repos/octocat/git-consortium/assignees{/user}\",\n" +
                "        \"branches_url\": \"https://api.github.com/repos/octocat/git-consortium/branches{/branch}\",\n" +
                "        \"tags_url\": \"https://api.github.com/repos/octocat/git-consortium/tags\",\n" +
                "        \"blobs_url\": \"https://api.github.com/repos/octocat/git-consortium/git/blobs{/sha}\",\n" +
                "        \"git_tags_url\": \"https://api.github.com/repos/octocat/git-consortium/git/tags{/sha}\",\n" +
                "        \"git_refs_url\": \"https://api.github.com/repos/octocat/git-consortium/git/refs{/sha}\",\n" +
                "        \"trees_url\": \"https://api.github.com/repos/octocat/git-consortium/git/trees{/sha}\",\n" +
                "        \"statuses_url\": \"https://api.github.com/repos/octocat/git-consortium/statuses/{sha}\",\n" +
                "        \"languages_url\": \"https://api.github.com/repos/octocat/git-consortium/languages\",\n" +
                "        \"stargazers_url\": \"https://api.github.com/repos/octocat/git-consortium/stargazers\",\n" +
                "        \"contributors_url\": \"https://api.github.com/repos/octocat/git-consortium/contributors\",\n" +
                "        \"subscribers_url\": \"https://api.github.com/repos/octocat/git-consortium/subscribers\",\n" +
                "        \"subscription_url\": \"https://api.github.com/repos/octocat/git-consortium/subscription\",\n" +
                "        \"commits_url\": \"https://api.github.com/repos/octocat/git-consortium/commits{/sha}\",\n" +
                "        \"git_commits_url\": \"https://api.github.com/repos/octocat/git-consortium/git/commits{/sha}\",\n" +
                "        \"comments_url\": \"https://api.github.com/repos/octocat/git-consortium/comments{/number}\",\n" +
                "        \"issue_comment_url\": \"https://api.github.com/repos/octocat/git-consortium/issues/comments{/number}\",\n" +
                "        \"contents_url\": \"https://api.github.com/repos/octocat/git-consortium/contents/{+path}\",\n" +
                "        \"compare_url\": \"https://api.github.com/repos/octocat/git-consortium/compare/{base}...{head}\",\n" +
                "        \"merges_url\": \"https://api.github.com/repos/octocat/git-consortium/merges\",\n" +
                "        \"archive_url\": \"https://api.github.com/repos/octocat/git-consortium/{archive_format}{/ref}\",\n" +
                "        \"downloads_url\": \"https://api.github.com/repos/octocat/git-consortium/downloads\",\n" +
                "        \"issues_url\": \"https://api.github.com/repos/octocat/git-consortium/issues{/number}\",\n" +
                "        \"pulls_url\": \"https://api.github.com/repos/octocat/git-consortium/pulls{/number}\",\n" +
                "        \"milestones_url\": \"https://api.github.com/repos/octocat/git-consortium/milestones{/number}\",\n" +
                "        \"notifications_url\": \"https://api.github.com/repos/octocat/git-consortium/notifications{?since,all,participating}\",\n" +
                "        \"labels_url\": \"https://api.github.com/repos/octocat/git-consortium/labels{/name}\",\n" +
                "        \"releases_url\": \"https://api.github.com/repos/octocat/git-consortium/releases{/id}\",\n" +
                "        \"deployments_url\": \"https://api.github.com/repos/octocat/git-consortium/deployments\",\n" +
                "        \"created_at\": \"2014-03-28T17:55:38Z\",\n" +
                "        \"updated_at\": \"2016-04-26T12:19:44Z\",\n" +
                "        \"pushed_at\": \"2015-10-28T23:30:54Z\",\n" +
                "        \"git_url\": \"git://github.com/octocat/git-consortium.git\",\n" +
                "        \"ssh_url\": \"git@github.com:octocat/git-consortium.git\",\n" +
                "        \"clone_url\": \"https://github.com/octocat/git-consortium.git\",\n" +
                "        \"svn_url\": \"https://github.com/octocat/git-consortium\",\n" +
                "        \"homepage\": null,\n" +
                "        \"size\": 190,\n" +
                "        \"stargazers_count\": 7,\n" +
                "        \"watchers_count\": 7,\n" +
                "        \"language\": null,\n" +
                "        \"has_issues\": true,\n" +
                "        \"has_downloads\": true,\n" +
                "        \"has_wiki\": true,\n" +
                "        \"has_pages\": false,\n" +
                "        \"forks_count\": 15,\n" +
                "        \"mirror_url\": null,\n" +
                "        \"open_issues_count\": 3,\n" +
                "        \"forks\": 15,\n" +
                "        \"open_issues\": 3,\n" +
                "        \"watchers\": 7,\n" +
                "        \"default_branch\": \"master\"\n" +
                "      }\n" +
                "    }\n" +
                "  },\n" +
                "  {\n" +
                "    \"url\": \"https://api.github.com/repos/octocat/git-consortium/pulls/6\",\n" +
                "    \"id\": 49050674,\n" +
                "    \"html_url\": \"https://github.com/octocat/git-consortium/pull/6\",\n" +
                "    \"diff_url\": \"https://github.com/octocat/git-consortium/pull/6.diff\",\n" +
                "    \"patch_url\": \"https://github.com/octocat/git-consortium/pull/6.patch\",\n" +
                "    \"issue_url\": \"https://api.github.com/repos/octocat/git-consortium/issues/6\",\n" +
                "    \"number\": 6,\n" +
                "    \"state\": \"open\",\n" +
                "    \"locked\": false,\n" +
                "    \"title\": \"Update LICENSE\",\n" +
                "    \"user\": {\n" +
                "      \"login\": \"IPyroJoe\",\n" +
                "      \"id\": 8882166,\n" +
                "      \"avatar_url\": \"https://avatars.githubusercontent.com/u/8882166?v=3\",\n" +
                "      \"gravatar_id\": \"\",\n" +
                "      \"url\": \"https://api.github.com/users/IPyroJoe\",\n" +
                "      \"html_url\": \"https://github.com/IPyroJoe\",\n" +
                "      \"followers_url\": \"https://api.github.com/users/IPyroJoe/followers\",\n" +
                "      \"following_url\": \"https://api.github.com/users/IPyroJoe/following{/other_user}\",\n" +
                "      \"gists_url\": \"https://api.github.com/users/IPyroJoe/gists{/gist_id}\",\n" +
                "      \"starred_url\": \"https://api.github.com/users/IPyroJoe/starred{/owner}{/repo}\",\n" +
                "      \"subscriptions_url\": \"https://api.github.com/users/IPyroJoe/subscriptions\",\n" +
                "      \"organizations_url\": \"https://api.github.com/users/IPyroJoe/orgs\",\n" +
                "      \"repos_url\": \"https://api.github.com/users/IPyroJoe/repos\",\n" +
                "      \"events_url\": \"https://api.github.com/users/IPyroJoe/events{/privacy}\",\n" +
                "      \"received_events_url\": \"https://api.github.com/users/IPyroJoe/received_events\",\n" +
                "      \"type\": \"User\",\n" +
                "      \"site_admin\": false\n" +
                "    },\n" +
                "    \"body\": \"\",\n" +
                "    \"created_at\": \"2015-10-28T18:52:59Z\",\n" +
                "    \"updated_at\": \"2015-10-28T18:52:59Z\",\n" +
                "    \"closed_at\": null,\n" +
                "    \"merged_at\": null,\n" +
                "    \"merge_commit_sha\": \"c3d71637171aa40320d52ff82df0a694cb8a7c0d\",\n" +
                "    \"assignee\": null,\n" +
                "    \"milestone\": null,\n" +
                "    \"commits_url\": \"https://api.github.com/repos/octocat/git-consortium/pulls/6/commits\",\n" +
                "    \"review_comments_url\": \"https://api.github.com/repos/octocat/git-consortium/pulls/6/comments\",\n" +
                "    \"review_comment_url\": \"https://api.github.com/repos/octocat/git-consortium/pulls/comments{/number}\",\n" +
                "    \"comments_url\": \"https://api.github.com/repos/octocat/git-consortium/issues/6/comments\",\n" +
                "    \"statuses_url\": \"https://api.github.com/repos/octocat/git-consortium/statuses/78846ad1aca35dcbd173e9d2214dacce709f52db\",\n" +
                "    \"head\": {\n" +
                "      \"label\": \"IPyroJoe:master\",\n" +
                "      \"ref\": \"master\",\n" +
                "      \"sha\": \"78846ad1aca35dcbd173e9d2214dacce709f52db\",\n" +
                "      \"user\": {\n" +
                "        \"login\": \"IPyroJoe\",\n" +
                "        \"id\": 8882166,\n" +
                "        \"avatar_url\": \"https://avatars.githubusercontent.com/u/8882166?v=3\",\n" +
                "        \"gravatar_id\": \"\",\n" +
                "        \"url\": \"https://api.github.com/users/IPyroJoe\",\n" +
                "        \"html_url\": \"https://github.com/IPyroJoe\",\n" +
                "        \"followers_url\": \"https://api.github.com/users/IPyroJoe/followers\",\n" +
                "        \"following_url\": \"https://api.github.com/users/IPyroJoe/following{/other_user}\",\n" +
                "        \"gists_url\": \"https://api.github.com/users/IPyroJoe/gists{/gist_id}\",\n" +
                "        \"starred_url\": \"https://api.github.com/users/IPyroJoe/starred{/owner}{/repo}\",\n" +
                "        \"subscriptions_url\": \"https://api.github.com/users/IPyroJoe/subscriptions\",\n" +
                "        \"organizations_url\": \"https://api.github.com/users/IPyroJoe/orgs\",\n" +
                "        \"repos_url\": \"https://api.github.com/users/IPyroJoe/repos\",\n" +
                "        \"events_url\": \"https://api.github.com/users/IPyroJoe/events{/privacy}\",\n" +
                "        \"received_events_url\": \"https://api.github.com/users/IPyroJoe/received_events\",\n" +
                "        \"type\": \"User\",\n" +
                "        \"site_admin\": false\n" +
                "      },\n" +
                "      \"repo\": {\n" +
                "        \"id\": 45134851,\n" +
                "        \"name\": \"git-consortium\",\n" +
                "        \"full_name\": \"IPyroJoe/git-consortium\",\n" +
                "        \"owner\": {\n" +
                "          \"login\": \"IPyroJoe\",\n" +
                "          \"id\": 8882166,\n" +
                "          \"avatar_url\": \"https://avatars.githubusercontent.com/u/8882166?v=3\",\n" +
                "          \"gravatar_id\": \"\",\n" +
                "          \"url\": \"https://api.github.com/users/IPyroJoe\",\n" +
                "          \"html_url\": \"https://github.com/IPyroJoe\",\n" +
                "          \"followers_url\": \"https://api.github.com/users/IPyroJoe/followers\",\n" +
                "          \"following_url\": \"https://api.github.com/users/IPyroJoe/following{/other_user}\",\n" +
                "          \"gists_url\": \"https://api.github.com/users/IPyroJoe/gists{/gist_id}\",\n" +
                "          \"starred_url\": \"https://api.github.com/users/IPyroJoe/starred{/owner}{/repo}\",\n" +
                "          \"subscriptions_url\": \"https://api.github.com/users/IPyroJoe/subscriptions\",\n" +
                "          \"organizations_url\": \"https://api.github.com/users/IPyroJoe/orgs\",\n" +
                "          \"repos_url\": \"https://api.github.com/users/IPyroJoe/repos\",\n" +
                "          \"events_url\": \"https://api.github.com/users/IPyroJoe/events{/privacy}\",\n" +
                "          \"received_events_url\": \"https://api.github.com/users/IPyroJoe/received_events\",\n" +
                "          \"type\": \"User\",\n" +
                "          \"site_admin\": false\n" +
                "        },\n" +
                "        \"private\": false,\n" +
                "        \"html_url\": \"https://github.com/IPyroJoe/git-consortium\",\n" +
                "        \"description\": \"This repo is for demonstration purposes only.\",\n" +
                "        \"fork\": true,\n" +
                "        \"url\": \"https://api.github.com/repos/IPyroJoe/git-consortium\",\n" +
                "        \"forks_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/forks\",\n" +
                "        \"keys_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/keys{/key_id}\",\n" +
                "        \"collaborators_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/collaborators{/collaborator}\",\n" +
                "        \"teams_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/teams\",\n" +
                "        \"hooks_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/hooks\",\n" +
                "        \"issue_events_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/issues/events{/number}\",\n" +
                "        \"events_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/events\",\n" +
                "        \"assignees_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/assignees{/user}\",\n" +
                "        \"branches_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/branches{/branch}\",\n" +
                "        \"tags_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/tags\",\n" +
                "        \"blobs_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/git/blobs{/sha}\",\n" +
                "        \"git_tags_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/git/tags{/sha}\",\n" +
                "        \"git_refs_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/git/refs{/sha}\",\n" +
                "        \"trees_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/git/trees{/sha}\",\n" +
                "        \"statuses_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/statuses/{sha}\",\n" +
                "        \"languages_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/languages\",\n" +
                "        \"stargazers_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/stargazers\",\n" +
                "        \"contributors_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/contributors\",\n" +
                "        \"subscribers_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/subscribers\",\n" +
                "        \"subscription_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/subscription\",\n" +
                "        \"commits_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/commits{/sha}\",\n" +
                "        \"git_commits_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/git/commits{/sha}\",\n" +
                "        \"comments_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/comments{/number}\",\n" +
                "        \"issue_comment_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/issues/comments{/number}\",\n" +
                "        \"contents_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/contents/{+path}\",\n" +
                "        \"compare_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/compare/{base}...{head}\",\n" +
                "        \"merges_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/merges\",\n" +
                "        \"archive_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/{archive_format}{/ref}\",\n" +
                "        \"downloads_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/downloads\",\n" +
                "        \"issues_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/issues{/number}\",\n" +
                "        \"pulls_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/pulls{/number}\",\n" +
                "        \"milestones_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/milestones{/number}\",\n" +
                "        \"notifications_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/notifications{?since,all,participating}\",\n" +
                "        \"labels_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/labels{/name}\",\n" +
                "        \"releases_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/releases{/id}\",\n" +
                "        \"deployments_url\": \"https://api.github.com/repos/IPyroJoe/git-consortium/deployments\",\n" +
                "        \"created_at\": \"2015-10-28T18:52:32Z\",\n" +
                "        \"updated_at\": \"2015-10-12T22:15:23Z\",\n" +
                "        \"pushed_at\": \"2015-10-28T18:52:47Z\",\n" +
                "        \"git_url\": \"git://github.com/IPyroJoe/git-consortium.git\",\n" +
                "        \"ssh_url\": \"git@github.com:IPyroJoe/git-consortium.git\",\n" +
                "        \"clone_url\": \"https://github.com/IPyroJoe/git-consortium.git\",\n" +
                "        \"svn_url\": \"https://github.com/IPyroJoe/git-consortium\",\n" +
                "        \"homepage\": null,\n" +
                "        \"size\": 99,\n" +
                "        \"stargazers_count\": 0,\n" +
                "        \"watchers_count\": 0,\n" +
                "        \"language\": null,\n" +
                "        \"has_issues\": false,\n" +
                "        \"has_downloads\": true,\n" +
                "        \"has_wiki\": true,\n" +
                "        \"has_pages\": false,\n" +
                "        \"forks_count\": 0,\n" +
                "        \"mirror_url\": null,\n" +
                "        \"open_issues_count\": 0,\n" +
                "        \"forks\": 0,\n" +
                "        \"open_issues\": 0,\n" +
                "        \"watchers\": 0,\n" +
                "        \"default_branch\": \"master\"\n" +
                "      }\n" +
                "    },\n" +
                "    \"base\": {\n" +
                "      \"label\": \"octocat:master\",\n" +
                "      \"ref\": \"master\",\n" +
                "      \"sha\": \"b33a9c7c02ad93f621fa38f0e9fc9e867e12fa0e\",\n" +
                "      \"user\": {\n" +
                "        \"login\": \"octocat\",\n" +
                "        \"id\": 583231,\n" +
                "        \"avatar_url\": \"https://avatars.githubusercontent.com/u/583231?v=3\",\n" +
                "        \"gravatar_id\": \"\",\n" +
                "        \"url\": \"https://api.github.com/users/octocat\",\n" +
                "        \"html_url\": \"https://github.com/octocat\",\n" +
                "        \"followers_url\": \"https://api.github.com/users/octocat/followers\",\n" +
                "        \"following_url\": \"https://api.github.com/users/octocat/following{/other_user}\",\n" +
                "        \"gists_url\": \"https://api.github.com/users/octocat/gists{/gist_id}\",\n" +
                "        \"starred_url\": \"https://api.github.com/users/octocat/starred{/owner}{/repo}\",\n" +
                "        \"subscriptions_url\": \"https://api.github.com/users/octocat/subscriptions\",\n" +
                "        \"organizations_url\": \"https://api.github.com/users/octocat/orgs\",\n" +
                "        \"repos_url\": \"https://api.github.com/users/octocat/repos\",\n" +
                "        \"events_url\": \"https://api.github.com/users/octocat/events{/privacy}\",\n" +
                "        \"received_events_url\": \"https://api.github.com/users/octocat/received_events\",\n" +
                "        \"type\": \"User\",\n" +
                "        \"site_admin\": false\n" +
                "      },\n" +
                "      \"repo\": {\n" +
                "        \"id\": 18221276,\n" +
                "        \"name\": \"git-consortium\",\n" +
                "        \"full_name\": \"octocat/git-consortium\",\n" +
                "        \"owner\": {\n" +
                "          \"login\": \"octocat\",\n" +
                "          \"id\": 583231,\n" +
                "          \"avatar_url\": \"https://avatars.githubusercontent.com/u/583231?v=3\",\n" +
                "          \"gravatar_id\": \"\",\n" +
                "          \"url\": \"https://api.github.com/users/octocat\",\n" +
                "          \"html_url\": \"https://github.com/octocat\",\n" +
                "          \"followers_url\": \"https://api.github.com/users/octocat/followers\",\n" +
                "          \"following_url\": \"https://api.github.com/users/octocat/following{/other_user}\",\n" +
                "          \"gists_url\": \"https://api.github.com/users/octocat/gists{/gist_id}\",\n" +
                "          \"starred_url\": \"https://api.github.com/users/octocat/starred{/owner}{/repo}\",\n" +
                "          \"subscriptions_url\": \"https://api.github.com/users/octocat/subscriptions\",\n" +
                "          \"organizations_url\": \"https://api.github.com/users/octocat/orgs\",\n" +
                "          \"repos_url\": \"https://api.github.com/users/octocat/repos\",\n" +
                "          \"events_url\": \"https://api.github.com/users/octocat/events{/privacy}\",\n" +
                "          \"received_events_url\": \"https://api.github.com/users/octocat/received_events\",\n" +
                "          \"type\": \"User\",\n" +
                "          \"site_admin\": false\n" +
                "        },\n" +
                "        \"private\": false,\n" +
                "        \"html_url\": \"https://github.com/octocat/git-consortium\",\n" +
                "        \"description\": \"This repo is for demonstration purposes only.\",\n" +
                "        \"fork\": false,\n" +
                "        \"url\": \"https://api.github.com/repos/octocat/git-consortium\",\n" +
                "        \"forks_url\": \"https://api.github.com/repos/octocat/git-consortium/forks\",\n" +
                "        \"keys_url\": \"https://api.github.com/repos/octocat/git-consortium/keys{/key_id}\",\n" +
                "        \"collaborators_url\": \"https://api.github.com/repos/octocat/git-consortium/collaborators{/collaborator}\",\n" +
                "        \"teams_url\": \"https://api.github.com/repos/octocat/git-consortium/teams\",\n" +
                "        \"hooks_url\": \"https://api.github.com/repos/octocat/git-consortium/hooks\",\n" +
                "        \"issue_events_url\": \"https://api.github.com/repos/octocat/git-consortium/issues/events{/number}\",\n" +
                "        \"events_url\": \"https://api.github.com/repos/octocat/git-consortium/events\",\n" +
                "        \"assignees_url\": \"https://api.github.com/repos/octocat/git-consortium/assignees{/user}\",\n" +
                "        \"branches_url\": \"https://api.github.com/repos/octocat/git-consortium/branches{/branch}\",\n" +
                "        \"tags_url\": \"https://api.github.com/repos/octocat/git-consortium/tags\",\n" +
                "        \"blobs_url\": \"https://api.github.com/repos/octocat/git-consortium/git/blobs{/sha}\",\n" +
                "        \"git_tags_url\": \"https://api.github.com/repos/octocat/git-consortium/git/tags{/sha}\",\n" +
                "        \"git_refs_url\": \"https://api.github.com/repos/octocat/git-consortium/git/refs{/sha}\",\n" +
                "        \"trees_url\": \"https://api.github.com/repos/octocat/git-consortium/git/trees{/sha}\",\n" +
                "        \"statuses_url\": \"https://api.github.com/repos/octocat/git-consortium/statuses/{sha}\",\n" +
                "        \"languages_url\": \"https://api.github.com/repos/octocat/git-consortium/languages\",\n" +
                "        \"stargazers_url\": \"https://api.github.com/repos/octocat/git-consortium/stargazers\",\n" +
                "        \"contributors_url\": \"https://api.github.com/repos/octocat/git-consortium/contributors\",\n" +
                "        \"subscribers_url\": \"https://api.github.com/repos/octocat/git-consortium/subscribers\",\n" +
                "        \"subscription_url\": \"https://api.github.com/repos/octocat/git-consortium/subscription\",\n" +
                "        \"commits_url\": \"https://api.github.com/repos/octocat/git-consortium/commits{/sha}\",\n" +
                "        \"git_commits_url\": \"https://api.github.com/repos/octocat/git-consortium/git/commits{/sha}\",\n" +
                "        \"comments_url\": \"https://api.github.com/repos/octocat/git-consortium/comments{/number}\",\n" +
                "        \"issue_comment_url\": \"https://api.github.com/repos/octocat/git-consortium/issues/comments{/number}\",\n" +
                "        \"contents_url\": \"https://api.github.com/repos/octocat/git-consortium/contents/{+path}\",\n" +
                "        \"compare_url\": \"https://api.github.com/repos/octocat/git-consortium/compare/{base}...{head}\",\n" +
                "        \"merges_url\": \"https://api.github.com/repos/octocat/git-consortium/merges\",\n" +
                "        \"archive_url\": \"https://api.github.com/repos/octocat/git-consortium/{archive_format}{/ref}\",\n" +
                "        \"downloads_url\": \"https://api.github.com/repos/octocat/git-consortium/downloads\",\n" +
                "        \"issues_url\": \"https://api.github.com/repos/octocat/git-consortium/issues{/number}\",\n" +
                "        \"pulls_url\": \"https://api.github.com/repos/octocat/git-consortium/pulls{/number}\",\n" +
                "        \"milestones_url\": \"https://api.github.com/repos/octocat/git-consortium/milestones{/number}\",\n" +
                "        \"notifications_url\": \"https://api.github.com/repos/octocat/git-consortium/notifications{?since,all,participating}\",\n" +
                "        \"labels_url\": \"https://api.github.com/repos/octocat/git-consortium/labels{/name}\",\n" +
                "        \"releases_url\": \"https://api.github.com/repos/octocat/git-consortium/releases{/id}\",\n" +
                "        \"deployments_url\": \"https://api.github.com/repos/octocat/git-consortium/deployments\",\n" +
                "        \"created_at\": \"2014-03-28T17:55:38Z\",\n" +
                "        \"updated_at\": \"2016-04-26T12:19:44Z\",\n" +
                "        \"pushed_at\": \"2015-10-28T23:30:54Z\",\n" +
                "        \"git_url\": \"git://github.com/octocat/git-consortium.git\",\n" +
                "        \"ssh_url\": \"git@github.com:octocat/git-consortium.git\",\n" +
                "        \"clone_url\": \"https://github.com/octocat/git-consortium.git\",\n" +
                "        \"svn_url\": \"https://github.com/octocat/git-consortium\",\n" +
                "        \"homepage\": null,\n" +
                "        \"size\": 190,\n" +
                "        \"stargazers_count\": 7,\n" +
                "        \"watchers_count\": 7,\n" +
                "        \"language\": null,\n" +
                "        \"has_issues\": true,\n" +
                "        \"has_downloads\": true,\n" +
                "        \"has_wiki\": true,\n" +
                "        \"has_pages\": false,\n" +
                "        \"forks_count\": 15,\n" +
                "        \"mirror_url\": null,\n" +
                "        \"open_issues_count\": 3,\n" +
                "        \"forks\": 15,\n" +
                "        \"open_issues\": 3,\n" +
                "        \"watchers\": 7,\n" +
                "        \"default_branch\": \"master\"\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "]";

        Type listType = new TypeToken<ArrayList<PullRequestResponse>>() {
        }.getType();
        List<PullRequestResponse> pullList = new Gson().fromJson(PULL_REQUESTS_MOCK, listType);

        Assert.assertNotNull(pullList);
    }
}
